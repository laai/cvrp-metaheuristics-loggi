# laai-cvrp image
# 
# A imagem Docker é usada para conter todos os executáveis do projeto e pode
# ser usada para disparar experimentos, ou seja, conjuntos de execuções de 
# algoritmos. Além disso, ela abstrai todo o processo de instalação e
# configuração de um setup de build para compilar o projeto. 
# É baseada no alpine o que faz com que possua um tamanho bem pequeno e seja
# mais fácil de subir.
FROM alpine:3.14 AS build
LABEL stage=build

RUN apk update; apk add bash gcc g++ libgcc libstdc++ musl gcompat expat-dev libtbb-dev \
 boost-dev curl unzip make cmake openssl-dev git lua5.2-dev rapidjson-dev \
 fmt-dev sqlite sqlite-dev sqlite-libs

WORKDIR /app
COPY . .

# Configure and Build: configure the system installing project dependencies manually
# and compiling all executable binaries.
RUN bash scripts/configure.sh
RUN bash scripts/build.sh

### Final image

FROM alpine:3.14
RUN apk add libgcc libstdc++ musl bash curl unzip sqlite3pp gcompat expat tzdata rapidjson lua5.2 boost fmt sqlite sqlite-dev sqlite-libs; \
    cp /usr/share/zoneinfo/America/Belem /etc/localtime; \
    echo "America/Belem" > /etc/timezone; \
    apk del tzdata

# Copy the 12 instances used in Master's Research
COPY --from=build /app/data/cvrp-instances-1.0/masters-thesis /instances/masters-thesis
COPY --from=build /app/resources /resources
COPY --from=build /app/solvecvrp /usr/bin/

COPY --from=build /app/scripts/download-osrm-data.sh /download-osrm-data.sh
COPY --from=build /app/scripts/run_experiments.sh /run_experiments.sh

RUN chmod +x /run_experiments.sh; chmod +x /download-osrm-data.sh
RUN ./download-osrm-data.sh /resources

ENV PROPERTIES_FILE=/resources/properties.toml
ENV LOGGIBUD_SCHEMAS_DIR=/resources/schemas
ENV OSRM_DATA_PATH=/resources/osrm/brazil-201110.osrm

CMD ["./run_experiments.sh"]
