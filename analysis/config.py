from matplotlib.axes import Axes

# Configura um Axes para um gráfico com dois boxplots
def config_axes_for_two_boxplots(ax: Axes) -> None:
    ax.grid(True, axis='y', linestyle=':')
    ax.set_xticks([1, 2])
    ax.set_xticklabels(['Algoritmo Genético', 'GRASP'])
    ax.set_ylabel('Distância final da solução (Km)')
    return