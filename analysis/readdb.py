import sqlite3 as lite
from string import Template
from pandas import DataFrame, read_sql_query


def read_mean_results(dbfilename: str, algorithm: str, instance: str):
    connection = lite.connect(dbfilename)
    query_template = Template(
        'SELECT final_kilometer FROM executions WHERE algorithm = "$algorithm" AND instance = "$instance"'
    )
    query = query_template.substitute({"algorithm": algorithm, "instance": instance})
    df = read_sql_query(query, connection)
    connection.close()

    return df['final_kilometer']