from typing import Tuple, Union
from matplotlib import pyplot
from matplotlib.axes import Axes
from matplotlib.figure import Figure
from .readdb import read_mean_results
from .config import config_axes_for_two_boxplots

dbfile = "./data/results-2022-04.db"
instance_file = "cvrp-0-pa-90"

def generate_general_boxplots_figure(
    dbfile: str,
    instance: str,
    figsize=(10, 8), save_as: Union[str, None] = None
) -> Tuple[Figure, Axes]:
    ga_results = read_mean_results(dbfile, "ga", instance)
    grasp_results = read_mean_results(dbfile, "grasp", instance)

    fig = pyplot.figure(figsize=figsize)
    axes: Axes = fig.add_subplot(111)
    axes.boxplot([ga_results, grasp_results], showmeans=True)
    config_axes_for_two_boxplots(axes)

    if save_as is not None:
        fig.savefig(str(save_as))

    return fig, axes

generate_general_boxplots_figure(dbfile, instance_file)
pyplot.show()
