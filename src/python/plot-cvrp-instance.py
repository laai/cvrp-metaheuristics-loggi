import argparse
import json
import folium
from typing import Dict


def plot_cvrp_instance(instance: dict):
    origin_lat = instance['origin']['lat']
    origin_lon = instance['origin']['lng']

    map = folium.Map(
        location=(origin_lat, origin_lon),
        tiles="cartodb positron",
        attr="My data Attribution")

    for delivery in instance["deliveries"]:
        lat = delivery["point"]["lat"]
        lon = delivery["point"]["lng"]
        folium.CircleMarker((lat, lon),
            color="black", 
            radius=1.25, 
            weight=2.5,
        ).add_to(map)

    folium.CircleMarker((origin_lat, origin_lon),
        color="orangered",
        radius=3,
        weight=6,
    ).add_to(map)

    return map


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("instance", type=str)
    args = parser.parse_args()

    instance: Dict

    with open(args.instance, "r") as f:
        instance = json.loads(f.read())

    map = plot_cvrp_instance(instance)
    map.save("instance.html")

