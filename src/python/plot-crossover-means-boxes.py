import os
import sqlite3
from argparser import get_default_argparser
from plotutils import plot_boxes, create_output_dir, instances_by_region


if __name__ == "__main__":
    parser = get_default_argparser(__file__)
    args = parser.parse_args()

    create_output_dir(args.output)

    width = int(args.width) if args.width else 1300
    height = int(args.height) if args.height else 800

    if args.db:
        cnx = sqlite3.connect(args.db)

        for region in instances_by_region.keys():
            all_image_name = f"./{args.output}/boxplots/all-crossovers-{region}.pdf"
            gox_gx_image_name = f"./{args.output}/boxplots/gox-gx-{region}.pdf"
            plot_boxes(cnx, instances_by_region[region], figsize=(width, height), imagename=all_image_name)
            plot_boxes(cnx, instances_by_region[region], figsize=(width, height), imagename=gox_gx_image_name, only_gox_and_gx=True)
