from sqlite3 import Connection
from time import sleep
from typing import List, Tuple
from pandas import DataFrame, concat, read_sql_query
from numpy import min as np_min, max as np_max
from plotly.graph_objects import Figure
import plotly.express as px
import plotly.io as pio
import matplotlib.pyplot as pyplot
import seaborn as sns
import numpy as np
import os
import json
import re

LABELS = dict(
    crossover_op="Crossover",
    final_total_distance="Distância Final (m)",
    instance_name="Instância"
)

COLORS = {
    'OX': '#F07B32',
    'CX': '#FFC300',
    'MOX': '#58D140',
    'LOX': '#4CC9F0',
    'PMX': '#3F37C9',
    'PPX': '#7209B7',
    'AEX': '#E493C5',
    'GOX': '#F3474C',
    'GX': '#780000',
    'GRASPBX': '#065a60'
}

instances_by_region = {
    "pa-0": ['cvrp-0-pa-100', 'cvrp-0-pa-116', 'cvrp-0-pa-117', 'cvrp-0-pa-118'],
    "df-0": ['cvrp-0-df-100', 'cvrp-0-df-30', 'cvrp-0-df-35', 'cvrp-0-df-42'],
    "rj-2": ['cvrp-2-rj-13', 'cvrp-2-rj-57', 'cvrp-2-rj-75', 'cvrp-2-rj-17']
}


def update_properties(f: Figure, is_bar: bool = False) -> Figure:
    if is_bar:
        for prop in f.data:
            if prop['name'] in COLORS.keys():
                prop['marker']['color'] = COLORS[prop['name']]

        return f

    for prop in f.data:
        if prop['name'] in COLORS.keys():
            prop['marker']['color'] = COLORS[prop['name']]
            prop['marker']['line'] = dict(color=COLORS[prop['name']], width=1)
            prop['line']['color'] = COLORS[prop['name']]
    
    f.update_traces(marker=dict(size=3, opacity=0.3))
    f.update_layout(
        font_size=13,
        font_family="Times New Roman", 
        legend=dict(
            itemsizing='constant'
        ),
    )
    
    return f


def mkdir(dirname: str):
    try:
        os.mkdir(dirname)
    except FileExistsError:
        pass


def create_output_dir(dirname: str):
    mkdir(dirname)
    mkdir(dirname + "/strip-charts")
    mkdir(dirname + "/boxplots")
    mkdir(dirname + "/convergences")
    mkdir(dirname + "/runtimes")


def build_sql_query(instances: List[str]) -> str:
    joined_instance_names = ", ".join([f'"{i}"' for i in instances])
    return f"""select instance_name, crossover_op, final_total_distance from experiment_results where instance_name IN ({joined_instance_names}) """


def filter_only_gox_and_gx(d: DataFrame) -> DataFrame:
    gox_view = d.loc[d['crossover_op'] == 'GOX']
    gx_view = d.loc[d['crossover_op'] == 'GX']
    return concat([gox_view, gx_view])


def get_y_range(d: DataFrame) -> Tuple[int, int]:
    min_y = int(np_min(d['final_total_distance']) / 1e+5 - 2) * 1e+5
    max_y = int(np_max(d['final_total_distance']) / 1e+5 + 2) * 1e+5
    return min_y, max_y


def plot_boxes(cnx: Connection, instance_names: List[str], figsize: Tuple[int, int], imagename: str, only_gox_and_gx: bool = False):
    pio.templates.default = "simple_white"
    px.defaults.template = "ggplot2"

    query = build_sql_query(instance_names)
    df = read_sql_query(query, cnx)

    if only_gox_and_gx:
        df = filter_only_gox_and_gx(df)

    min_y, max_y = get_y_range(df)
    is_rj = re.match(r".*-rj-.*", instance_names[0])

    # Create Figure
    fig = px.box(
        df,
        x='crossover_op',
        y='final_total_distance',
        color='crossover_op',
        facet_col='instance_name',
        labels=LABELS,
        width=figsize[0] * (1.5 if is_rj else 1),
        height=figsize[1] * (1.33 if only_gox_and_gx else 2),
        range_y=(min_y, max_y),
        facet_col_wrap=4 if only_gox_and_gx else 2,
        facet_col_spacing=0.01,
        points='all' if only_gox_and_gx else None,
    )

    update_properties(fig)
    fig.write_image(imagename)
    sleep(0.5)
    fig.write_image(imagename)
    return fig


def plot_strip_charts(cnx: Connection, instance_names: List[str], figsize: Tuple[int, int], imagename: str, only_gox_and_gx: bool = False):
    pio.templates.default = "simple_white"
    px.defaults.template = "ggplot2"

    query = build_sql_query(instance_names)
    df = read_sql_query(query, cnx)

    if only_gox_and_gx:
        df = filter_only_gox_and_gx(df)

    min_y, max_y = get_y_range(df)

    # Create Figure
    fig = px.strip(
        df,
        x='crossover_op',
        y='final_total_distance',
        color='crossover_op',
        facet_col='instance_name',
        labels=LABELS,
        width=figsize[0],
        height=figsize[1] * (1.25 if only_gox_and_gx else 2),
        range_y=(min_y, max_y),
        facet_col_spacing=0.01,
        facet_col_wrap=4 if only_gox_and_gx else 2,
    )

    update_properties(fig)
    fig.write_image(imagename)
    sleep(0.5)
    fig.write_image(imagename)


def format_dataframe(d: DataFrame) -> DataFrame:
    data = {
        'Geração': [],
        'Crossover': [],
        'Distância': [],
        'Instância': [],
    }
    for _, row in d.iterrows():
        convergence_list = json.loads(row['convergence'])
        for i in range(len(convergence_list)):
            data['Crossover'].append(row['crossover_op'])
            data['Distância'].append(convergence_list[i])
            data['Geração'].append(i + 1)
            data['Instância'].append(row['instance_name'])
    
    df = DataFrame(data)
    ox = df.loc[df['Crossover'] == 'OX']
    cx = df.loc[df['Crossover'] == 'CX']
    mox = df.loc[df['Crossover'] == 'MOX']
    lox = df.loc[df['Crossover'] == 'LOX']
    pmx = df.loc[df['Crossover'] == 'PMX']
    ppx = df.loc[df['Crossover'] == 'PPX']
    aex = df.loc[df['Crossover'] == 'AEX']
    gox = df.loc[df['Crossover'] == 'GOX']
    gx = df.loc[df['Crossover'] == 'GX']
    graspbx = df.loc[df['Crossover'] == 'GRASPBX']
    return concat([ox, cx, mox, lox, pmx, ppx, aex, gox, gx, graspbx], ignore_index=True)


def plot_convergences(cnx: Connection, region: str, figsize: Tuple[int, int], file: str):
    sns.set_theme(style='white', font='serif')

    query = f"""select instance_name, crossover_op, convergence from experiment_results where instance_region = "{region}" """
    df = read_sql_query(query, cnx)
    instances = df.groupby('instance_name').count().index.to_list()
    data = format_dataframe(df)

    fig, axes = pyplot.subplots(2, 2, figsize=figsize, sharex=True)

    for i in range(2):
        for j in range(2):
            current_instance = instances[i * 2 + j]
            current_data = data.loc[data['Instância'] == current_instance]
            max_ = np.max(current_data['Distância'])
            variance_ = (max_ // 20) / 1e+5
            variance_ = int(variance_) * 1e+5
            sns.lineplot(
                data=current_data,
                x='Geração',
                y='Distância',
                hue='Crossover',
                palette=list(COLORS.values()),
                ax=axes[i][j]
            )
            axes[i][j].set_xlim(0., max(data['Geração']))
            axes[i][j].set_ylabel('Distância (m)', fontdict=dict(family='serif', size=13))
            axes[i][j].set_title(f'Aplicados á instância {current_instance}', fontdict=dict(family='serif', size=13))
            axes[i][j].get_legend().remove()
            axes[i][j].set_yticks(np.arange(0., max_, variance_))

    handles, labels = axes[1][1].get_legend_handles_labels()
    fig.legend(handles, labels, loc='upper right')
    fig.subplots_adjust(top=0.95, bottom=0.05, left=0.07, right=0.93)

    fig.savefig(file)


def plot_runtime_bars(cnx: Connection, region: str, file: str, y_upper_limit: float = None, figsize = (1400, 500)):
    query = f"""select crossover_op, instance_region, instance_name, elapsed_time_ms from experiment_results er where instance_region = "{region}" """
    df = read_sql_query(query, cnx)

    crossovers = df['crossover_op'].drop_duplicates().to_list()
    data_list = []

    for crossover in crossovers:
        for instance in instances_by_region[region]:
            d = df.query(f'crossover_op == "{crossover}"').query(f'instance_name == "{instance}"').head(50)
            data_list.append(d)

    data = concat(data_list, ignore_index=True)
    data = data.groupby(by=['crossover_op', 'instance_name'], as_index=False).agg('mean')

    data.to_csv(file.replace("pdf", "csv"))

    fig = px.bar(data, 
        x="instance_name",
        y="elapsed_time_ms",
        color="crossover_op",
        barmode="group",
        width=figsize[0],
        height=figsize[1],
        template='simple_white',
        text_auto='.2s',
        labels={
            'crossover_op': 'Cruzamento',
            'elapsed_time_ms': 'Tempos de Execução (ms)',
            'instance_name': 'Instância'
        }
    )
    
    update_properties(fig, is_bar=True)
    fig.update_traces(textfont_size=12, textangle=0, textposition="outside", cliponaxis=False)
    if y_upper_limit is not None:
        fig.update_yaxes(range=[0, y_upper_limit])
    fig.write_image(file)
    
