import sqlite3
from argparser import get_default_argparser
from plotutils import plot_convergences, create_output_dir


if __name__ == "__main__":
    parser = get_default_argparser(__file__)
    args = parser.parse_args()

    create_output_dir(args.output)

    default_fig_size = figsize=(15, 12)

    if args.db:
        cnx = sqlite3.connect(args.db)

        for region in ["pa-0", "df-0", "rj-2"]:
            print(f"Plotando convergências para as instâncias da região {region}")
            plot_convergences(cnx, region, figsize=default_fig_size, file=f"{args.output}/convergences/{region}.pdf")
