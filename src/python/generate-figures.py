import sqlite3
from argparser import get_default_argparser
from plotutils import create_output_dir, plot_boxes, plot_strip_charts, plot_convergences, plot_runtime_bars, instances_by_region

elapsed_time_upper_limits = {
    'pa-0': 2e+3,
    'df-0': 3e+4,
    'rj-2': 14e+4
}

if __name__ == "__main__":
    parser = get_default_argparser(__file__)
    args = parser.parse_args()

    create_output_dir(args.output)

    width = int(args.width) if args.width else 1100
    height = int(args.height) if args.height else 400

    if args.db:
        cnx = sqlite3.connect(args.db)

        for region in instances_by_region.keys():
            h = 1. if region != 'rj-2' else 1.5

            # Plot the Boxplots
            all_image_name = f"./{args.output}/boxplots/all-crossovers-{region}.pdf"
            gox_gx_image_name = f"./{args.output}/boxplots/gox-gx-{region}.pdf"
            print(f"Plotando Boxplots de resultados de todos os crossovers para as instâncias da região {region}")
            plot_boxes(cnx, instances_by_region[region], figsize=(width, height*h), imagename=all_image_name)
            print(f"Plotando Boxplots de resultados finais dos crossovers gulosos para as instâncias da região {region}")
            plot_boxes(cnx, instances_by_region[region], figsize=(width, height*h), imagename=gox_gx_image_name, only_gox_and_gx=True)

            # Plot the Strip Charts
            all_image_name = f"./{args.output}/strip-charts/all-crossovers-{region}.pdf"
            gox_gx_image_name = f"./{args.output}/strip-charts/gox-gx-{region}.pdf"
            print(f"Plotando Strip Charts de resultados de todos os crossovers para as instâncias da região {region}")
            plot_strip_charts(cnx, instances_by_region[region], figsize=(width, height), imagename=all_image_name)
            print(f"Plotando Strip Charts de resultados finais dos crossovers gulosos para as instâncias da região {region}")
            plot_strip_charts(cnx, instances_by_region[region], figsize=(width, height), imagename=gox_gx_image_name, only_gox_and_gx=True)

            # Plot the Convergences
            print(f"Plotando convergências para as instâncias da região {region}")
            plot_convergences(cnx, region, figsize=(14, 12), file=f"{args.output}/convergences/{region}.pdf")

            # Plot run times
            print(f"Plotando os tempos de execução em barras")
            plot_runtime_bars(cnx, region, 
                file=f"{args.output}/runtimes/run-times-{region}.pdf",
                y_upper_limit=elapsed_time_upper_limits[region],
                figsize=(1500, 500),
            )
