import sqlite3
from plotting.mean_plots import plot_crossover_mean_results

query = """select crossover_op, avg(final_total_distance) as mean_distance, count(id) as "count" from experiment_results where instance_name = "cvrp-0-pa-116" group by crossover_op;"""

if __name__ == "__main__":
    cnx = sqlite3.connect("../../first-results.db")
    
    plot_crossover_mean_results(cnx, "cvrp-0-pa-116")
