from argparse import ArgumentParser

def get_default_argparser(name: str):
    parser = ArgumentParser(name)
    parser.add_argument("-d", "--db", type=str, required=True)
    parser.add_argument("--width", type=int)
    parser.add_argument("--height", type=int)
    parser.add_argument("--output", type=str, default="figures")
    return parser
