import sqlite3
from argparser import get_default_argparser
from plotutils import plot_runtime_bars, create_output_dir


if __name__ == "__main__":
    parser = get_default_argparser(__file__)
    parser.add_argument('-f', '--file', type=str)
    args = parser.parse_args()

    create_output_dir(args.output)

    width = int(args.width) if args.width else 1300
    height = int(args.height) if args.height else 800

    if args.db:
        cnx = sqlite3.connect(args.db)

        plot_runtime_bars(cnx, "rj-2", figsize=(width, height), file=args.file)