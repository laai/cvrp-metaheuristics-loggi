#ifndef CONTROLLERS_CONFIG_HPP_
#define CONTROLLERS_CONFIG_HPP_

#include <toml++/toml.h>

#include <string>

namespace controllers {

class IConfig {
 public:
  virtual std::string getString(const char*) = 0;
  virtual int getInteger(const char*) = 0;
  virtual unsigned long getULong(const char*) = 0;
  virtual double getDouble(const char*) = 0;
};

class TomlConfig : public IConfig {
 public:
  TomlConfig() {}
  TomlConfig(toml::table configTable) : configTable(configTable) {}

  std::string getString(const char*);
  int getInteger(const char*);
  double getDouble(const char*);
  unsigned long getULong(const char*);
  unsigned long getPopSize();
  unsigned long getTournamentSize();
  double getCrossingRate();
  double getMutationRate();
  unsigned long getPriorityListSize();
  unsigned long getNeighborhoodSearchSize();
  unsigned long getGensUntilTurnover();
  unsigned long getGOXSortingBufferSize();
  std::string getDatabaseFilename();

 private:
  toml::table configTable;
};

TomlConfig loadConfiguration(const char*);

}  // namespace controllers

#endif  // CONTROLLERS_CONFIG_HPP_
