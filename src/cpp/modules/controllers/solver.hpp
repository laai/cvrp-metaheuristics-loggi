#ifndef CONTROLLERS_SOLVER_HPP_
#define CONTROLLERS_SOLVER_HPP_

#include <loggibud/cvrp_instance.h>
#include <loggibud/cvrp_solution.h>

namespace controllers {

class Solver {
 public:
  virtual loggibud::CVRPSolution solve(loggibud::CVRPInstance const*) = 0;
  virtual const char* getName() = 0;
};

}  // namespace controllers

#endif  // CONTROLLERS_SOLVER_HPP_
