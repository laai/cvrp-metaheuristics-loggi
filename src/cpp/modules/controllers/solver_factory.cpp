#include "solver_factory.hpp"

#include <spdlog/logger.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/spdlog.h>

#include <cstring>
#include <metaheuristics/ga/crossovers/crossover_factory_strategies.hpp>
#include <metaheuristics/ga/solver_factory.hpp>
#include <metaheuristics/grasp/solver_factory.hpp>
#include <metaheuristics/hybrid/solver_factory.hpp>

using namespace controllers;

void logGraspArt();
void logGeneticAlgorithmArt();
void logHybridAlgorithmArt();

const char *errorMessage(const char *key) {
  std::string s = "error: ";
  s += "the key \"" + std::string(key) + "\" is unknown as a Solver";
  return s.c_str();
}

bool controllers::is_valid_crossover(std::string key) {
  auto crossovers = metaheuristics::ga_crossovers::getAllCrossoverIDs();
  bool result = false;
  for (auto xover : crossovers) {
    result = result || key.compare(xover) == 0;
  }
  return result;
}

SolverFactory *SolverFactory::getFactory(const char *key, TomlConfig *cfg, std::string &crossover) {
  if (std::strcmp("ga", key) == 0) {
    logGeneticAlgorithmArt();
    return new metaheuristics::SGASolverFactory(cfg, crossover);
  } else if (std::strcmp("grasp", key) == 0) {
    logGraspArt();
    return new metaheuristics::GRASPSolverFactory(cfg);
  } else if (std::strcmp("hybrid", key) == 0) {
    logHybridAlgorithmArt();
    return new metaheuristics::HybridSolverFactory(cfg);
  } else {
    throw std::logic_error(errorMessage(key));
  }
}

void logGraspArt() {
  spdlog::info("╔═╗ ╦═╗ ╔═╗ ╔═╗ ╔═╗");
  spdlog::info("║ ╦ ╠╦╝ ╠═╣ ╚═╗ ╠═╝");
  spdlog::info("╚═╝o╩╚═o╩ ╩o╚═╝o╩o ");
}

void logGeneticAlgorithmArt() {
  spdlog::info("╔═╗╔═╗╔╗╔╔═╗╔╦╗╦╔═╗  ╔═╗╦  ╔═╗╔═╗╦═╗╦╔╦╗╦ ╦╔╦╗");
  spdlog::info("║ ╦║╣ ║║║║╣  ║ ║║    ╠═╣║  ║ ╦║ ║╠╦╝║ ║ ╠═╣║║║");
  spdlog::info("╚═╝╚═╝╝╚╝╚═╝ ╩ ╩╚═╝  ╩ ╩╩═╝╚═╝╚═╝╩╚═╩ ╩ ╩ ╩╩ ╩");
}

void logHybridAlgorithmArt() {
  spdlog::info("╦ ╦╦ ╦╔╗ ╦═╗╦╔╦╗  ╔═╗╦  ╔═╗╔═╗╦═╗╦╔╦╗╦ ╦╔╦╗");
  spdlog::info("╠═╣╚╦╝╠╩╗╠╦╝║ ║║  ╠═╣║  ║ ╦║ ║╠╦╝║ ║ ╠═╣║║║");
  spdlog::info("╩ ╩ ╩ ╚═╝╩╚═╩═╩╝  ╩ ╩╩═╝╚═╝╚═╝╩╚═╩ ╩ ╩ ╩╩ ╩");
}
