#include "invoker.hpp"

#include <loggibud/cvrp_solution.h>
#include <spdlog/spdlog.h>

#include <chrono>
#include <sstream>
#include <string>
#include <time.hpp>
using namespace std::chrono_literals;
using std::chrono::duration_cast;
using std::chrono::hours;
using std::chrono::milliseconds;
using std::chrono::minutes;
using std::chrono::seconds;

#include "execution_data.hpp"
using helpers::time::getElapsedTime;

std::string format_milliseconds(milliseconds);

controllers::Invoker::Invoker(controllers::Solver* solver) : solver(solver) {}

int controllers::Invoker::invoke(loggibud::CVRPInstance const* instance) {
  controllers::ExecutionData* executionData = controllers::ExecutionData::getInstance();
  auto logger = spdlog::get("controllers::Invoker");
  loggibud::CVRPSolution solution;

  spdlog::info("Starting solving the instance {}", instance->name);
  milliseconds elapsedTime = getElapsedTime<milliseconds>([&]() { solution = solver->solve(instance); });

  spdlog::info("Solver elapsed time: {} ms\t{}", elapsedTime.count(), format_milliseconds(elapsedTime));
  spdlog::info("Solution has {} vehicles", solution.vehicles.size());

  size_t nDeliveries, occupation;
  for (size_t idx = 0; idx < solution.vehicles.size(); idx++) {
    nDeliveries = solution.vehicles[idx].deliveries.size();
    occupation = solution.vehicles[idx].occupation();
    spdlog::info("Vehicle #{}: {} deliveries; Occupation = {}", idx + 1, nDeliveries, occupation);
  }

  executionData->setElapsedTime(elapsedTime);
  executionData->setCVRPInstance((loggibud::CVRPInstance&)*instance);
  executionData->setCVRPSolution(solution);
  executionData->setAlgorithmName(solver->getName());

  return 0;
}

std::string format_milliseconds(milliseconds ms) {
  std::stringstream stream;
  const auto hrs = duration_cast<hours>(ms);
  const auto mins = duration_cast<minutes>(ms - hrs);
  const auto secs = duration_cast<seconds>(ms - hrs - mins);
  const auto _ms = duration_cast<milliseconds>(ms - hrs - mins - secs);

  if (hrs.count() > 0) stream << hrs.count() << "h ";
  if (mins.count() > 0) stream << mins.count() << "min ";
  if (secs.count() > 0) stream << secs.count() << "." << _ms.count() << "s";

  return stream.str();
}
