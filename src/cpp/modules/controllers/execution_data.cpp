#include "execution_data.hpp"

#include <json.hpp>
#include <sstream>

using namespace controllers;

ExecutionData* ExecutionData::INSTANCE = nullptr;

void ExecutionData::setId(int id) { this->result.id = id; }

void ExecutionData::setAlgorithmName(std::string name) { this->result.algorithmName = name; }

void ExecutionData::setAlgorithmProperties(std::string props) { this->result.algorithmProperties = props; }

void ExecutionData::setTotalDistance(float finalTotalDistance) { this->result.finalTotalDistance = finalTotalDistance; }

void ExecutionData::setElapsedTime(int ms) { this->result.elapsedTimeMs = ms; }

void ExecutionData::setElapsedTime(std::chrono::milliseconds ms) { this->result.elapsedTimeMs = ms.count(); }

void ExecutionData::setConvergence(std::string convergence) { this->result.convergence = convergence; }

void ExecutionData::setConvergence(std::vector<double>& convergence) {
  this->result.convergence = helpers::json::jsonifyConvergence(convergence);
}

void ExecutionData::setCVRPInstance(loggibud::CVRPInstance& cvrpInstance) {
  this->result.instance = helpers::json::jsonifyCvrpInstance(cvrpInstance);
}

void ExecutionData::setCVRPSolution(loggibud::CVRPSolution& cvrpSolution) {
  this->result.solution = helpers::json::jsonifyCvrpSolution(cvrpSolution);
}

void ExecutionData::setCrossover(std::string crossoverOp) { this->result.crossover = crossoverOp; }

void ExecutionData::setSolutionIndices(metaheuristics::Encoding& enc) {
  std::stringstream ss;
  ss << "[";
  for (size_t i = 0; i < enc.size(); i++) {
    ss << enc[i];
    if (i < enc.size() - 1) {
      ss << ",";
    }
  }
  ss << "]";
  this->result.solutionIndices = ss.str();
}