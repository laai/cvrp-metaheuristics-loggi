#ifndef CONTROLLERS_INVOKER_HPP_
#define CONTROLLERS_INVOKER_HPP_

#include <loggibud/cvrp_instance.h>
#include <spdlog/logger.h>
#include <spdlog/sinks/stdout_color_sinks.h>

#include "solver.hpp"

namespace controllers {

class Invoker {
 public:
  Invoker(Solver*);
  int invoke(loggibud::CVRPInstance const*);

 private:
  Solver* solver;
};

}  // namespace controllers

#endif  // CONTROLLERS_INVOKER_HPP_
