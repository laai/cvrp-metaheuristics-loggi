#ifndef CONTROLLERS_EXECUTION_DATA_HPP_
#define CONTROLLERS_EXECUTION_DATA_HPP_

#include <loggibud/cvrp_instance.h>
#include <loggibud/cvrp_solution.h>
#include <sqlite3pp.h>

#include <chrono>
#include <metaheuristics/encoding.hpp>
#include <repository/models.hpp>
#include <repository/result_repository.hpp>
#include <vector>

namespace controllers {

class ExecutionData {
 private:
  static ExecutionData* INSTANCE;
  repository::models::ExperimentResult result;

  ExecutionData() {}

 public:
  static ExecutionData* getInstance() {
    if (INSTANCE == nullptr) {
      INSTANCE = new ExecutionData();
    }
    return INSTANCE;
  }

  static void save(sqlite3pp::database* db) {
    if (INSTANCE != nullptr) {
      repository::ResultRepository(db).save(INSTANCE->result);
    }
  }

  static void reset() { INSTANCE = new ExecutionData(); }

  void setId(int id);
  void setAlgorithmName(std::string name);
  void setAlgorithmProperties(std::string props);
  void setTotalDistance(float finalTotalDistance);
  void setElapsedTime(int ms);
  void setElapsedTime(std::chrono::milliseconds ms);
  void setConvergence(std::string convergence);
  void setConvergence(std::vector<double>& convergence);
  void setCVRPInstance(loggibud::CVRPInstance& cvrpInstance);
  void setCVRPSolution(loggibud::CVRPSolution& cvrpSolution);
  void setCrossover(std::string crossoverOp);
  void setSolutionIndices(metaheuristics::Encoding& enc);
};

}  // namespace controllers

#endif  // CONTROLLERS_EXECUTION_DATA_HPP_
