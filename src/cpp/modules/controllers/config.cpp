#include "config.hpp"

#define GA_SECTION "ga"
#define GRASP_SECTION "grasp"
#define HYBRID_SECTION "hybrid-algorithm"

using namespace controllers;

TomlConfig controllers::loadConfiguration(const char* filename) {
  try {
    auto tbl = toml::parse_file(filename);
    return TomlConfig(tbl);
  } catch (std::exception& e) {
    std::string message("Error on parse configuration file: ");
    message += std::string(e.what());
    throw std::runtime_error(message);
  }
}

std::string TomlConfig::getString(const char* key) {
  return this->configTable[key].value_or<std::string>("");
}

int TomlConfig::getInteger(const char* key) { return this->configTable[key].value_or<int>(0); }

unsigned long TomlConfig::getULong(const char* key) {
  return this->configTable[key].value_or<unsigned long>(0L);
}

double TomlConfig::getDouble(const char* key) {
  return this->configTable[key].value_or<double>(0.0);
}

unsigned long TomlConfig::getPopSize() {
  return this->configTable[GA_SECTION]["pop-size"].value_or<unsigned long>(100L);
}

unsigned long TomlConfig::getTournamentSize() {
  return this->configTable[GA_SECTION]["tournament-size"].value_or<unsigned long>(4);
}

double TomlConfig::getCrossingRate() {
  return this->configTable[GA_SECTION]["crossing-rate"].value_or<double>(1.0);
}

double TomlConfig::getMutationRate() {
  return this->configTable[GA_SECTION]["mutation-rate"].value_or<double>(0.01);
}

unsigned long TomlConfig::getPriorityListSize() {
  return this->configTable[GRASP_SECTION]["priority-list-size"].value_or<unsigned long>(10L);
}

unsigned long TomlConfig::getNeighborhoodSearchSize() {
  return this->configTable[GRASP_SECTION]["neighborhood-search-size"].value_or<unsigned long>(10L);
}

unsigned long TomlConfig::getGensUntilTurnover() {
  return this->configTable[HYBRID_SECTION]["generations-until-turnover"].value_or<unsigned long>(
      10L);
}

std::string TomlConfig::getDatabaseFilename() {
  return this->configTable["output"].value_or<std::string>("database.db3");
}

unsigned long TomlConfig::getGOXSortingBufferSize() {
  return this->configTable["gox-sorting-buffer"].value_or<unsigned long>(20);
}
