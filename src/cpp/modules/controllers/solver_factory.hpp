#ifndef CONTROLLERS_SOLVER_FACTORY_HPP_
#define CONTROLLERS_SOLVER_FACTORY_HPP_

#include <loggibud/cvrp_instance.h>

#include <cvrp/adjacency_matrix.hpp>

#include "config.hpp"
#include "solver.hpp"

namespace controllers {

bool is_valid_crossover(std::string key);

class SolverFactory {
 public:
  static SolverFactory* getFactory(const char*, TomlConfig*, std::string&);
  virtual Solver* createSolver(loggibud::CVRPInstance const* instance, const cvrp::graphs::Matrix* matrix) = 0;
};

}  // namespace controllers

#endif  // CONTROLLERS_SOLVER_FACTORY_HPP_
