#ifndef METAHEURISTICS_HYBRID_POP_BASED_RESTRICT_LIST_HPP_
#define METAHEURISTICS_HYBRID_POP_BASED_RESTRICT_LIST_HPP_

#include <loggibud/cvrp_instance.h>
#include <loggibud/point.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/spdlog.h>

#include "../encoding.hpp"
#include "../grasp/restrict_list.hpp"

namespace metaheuristics {

class PopBasedRestrictList : public RestrictList {
 public:
  PopBasedRestrictList(const size_t, cvrp::RoutingService *);
  PopBasedRestrictList(const size_t, cvrp::RoutingService *, Population *);
  void setPopulation(Population *);
  void clear();
  void populate(const Encoding &, loggibud::Point &, loggibud::CVRPInstance &, set<size_t>);
  size_t choiceOne();

 private:
  const Population *populationPtr;

  // TODO: Implementar PopBasedRestrictList::getIndexDistanceBasedScore()
  //
  // Esse método deve gerar um Score gerado a partir de um cálculo baseado nas distâncias entre dois
  // valores dentro da permutação e nos fitness, isso deve ser calculado considerando todos os
  // indivíduos da população. Ou seja, para cada indivíduo, calcular a distância entre o valor A e B
  // e relacionar a distância com o fitness.
  double getIndexDistanceBasedScore();

  std::vector<int> getNumberOfOccurrenciesOfValue(size_t);
};

}  // namespace metaheuristics

#endif  // METAHEURISTICS_HYBRID_POP_BASED_RESTRICT_LIST_HPP_
