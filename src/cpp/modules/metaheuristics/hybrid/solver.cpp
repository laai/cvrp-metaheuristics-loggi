#include "solver.hpp"

#include <paradiseo/eo/apply.h>

#include <controllers/execution_data.hpp>

#include "../encoding.hpp"
#include "../encoding_builder.hpp"

using namespace metaheuristics;

const char* HybridSolver::getName() { return "hybrid"; }

HybridSolver::HybridSolver(size_t popSize, HybridAlgorithm alg, cvrp::DecodingService* decoder)
    : popSize(popSize), alg(alg), decoder(decoder) {}

loggibud::CVRPSolution HybridSolver::solve(loggibud::CVRPInstance const* instance) {
  Population population =
      EncodingBuilder(instance->deliveries.size()).buildPopulation(this->popSize);
  this->alg(population);
  Encoding best = population.best_element();
  controllers::ExecutionData::getInstance()->setTotalDistance(1 / best.fitness());
  return this->decoder->decode(best);
}
