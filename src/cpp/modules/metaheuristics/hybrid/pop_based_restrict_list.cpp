#include "pop_based_restrict_list.hpp"

#include <algorithm>
#include <cmath>

using namespace metaheuristics;
using loggibud::CVRPInstance;
using loggibud::Point;

PopBasedRestrictList::PopBasedRestrictList(const size_t size, cvrp::RoutingService *routingService)
    : RestrictList(size, routingService), populationPtr(nullptr) {}

PopBasedRestrictList::PopBasedRestrictList(const size_t size, cvrp::RoutingService *routingService,
                                           Population *pop)
    : RestrictList(size, routingService), populationPtr(pop) {}

void PopBasedRestrictList::setPopulation(Population *pop) { this->populationPtr = pop; }

void PopBasedRestrictList::clear() { this->list.clear(); }

void PopBasedRestrictList::populate(const Encoding &partialSolution, Point &point,
                                    CVRPInstance &instance, set<size_t> unavailable = {}) {
  // TODO: Finalizar a implementação do populate

  auto distancesMap = getDistancesToAllPoints(point, instance, unavailable);
  auto distancesVec = std::vector<Item>(distancesMap.begin(), distancesMap.end());
  auto countOccurrencesVec = getNumberOfOccurrenciesOfValue(partialSolution.size());

  for (Item &item : distancesVec) {
    double distance = item.second;
    int count = countOccurrencesVec.at(item.first);

    // Formula: d * c
    item.second = distance * count;
  }

  std::sort(distancesVec.begin(), distancesVec.end(),
            [](Item &a, Item &b) { return a.second < b.second; });
  auto N = distancesVec.size() < this->size_ ? distancesVec.size() : size_;
  this->list = vector<Item>(distancesVec.begin(), distancesVec.begin() + N);
  return;
}

size_t PopBasedRestrictList::choiceOne() {
  // TODO: Implementar especialização de choiceOne para PopBasedRestrictList
  return RestrictList::choiceOne();
}

std::vector<int> PopBasedRestrictList::getNumberOfOccurrenciesOfValue(size_t position) {
  const size_t chromSize = populationPtr->at(0).size();
  std::vector<int> count(chromSize);
  size_t enc_index = 0;
  for (; enc_index < populationPtr->size(); enc_index++) {
    size_t gene = populationPtr->at(enc_index).at(position);
    count[gene] += 1;
  }
  return count;
}

/**
 * Get indices of two values in a vector.
 * Receive two values and a vector and, search the indices of each value in the vector.
 *
 * @param a first value
 * @param b second value
 * @param vec reference to the vector
 * @return a pair of size_t values (both indices)
 */
// std::pair<size_t, size_t> PopBasedRestrictList::getIndicesOf(size_t a, size_t b,
//                                                              std::vector<size_t> &vec) {
//   size_t index_a, index_b;
//   bool found_a = false, found_b = false;

//   for (size_t i = 0; i < vec.size(); i++) {
//     if (a == vec[i]) {
//       index_a = i;
//       found_a = true;
//     }
//     if (b == vec[i]) {
//       index_b = i;
//       found_b = true;
//     }
//     if (found_a && found_b) {
//       break;
//     }
//   }

//   return std::pair<size_t, size_t>(index_a, index_b);
// }
