#ifndef METAHEURISTICS_HYBRID_ALGORITHM_HPP_
#define METAHEURISTICS_HYBRID_ALGORITHM_HPP_

#include <paradiseo/eo/eoPop.h>
#include <spdlog/spdlog.h>

#include <controllers/execution_data.hpp>

#include "../convergence_storage_eval_continue.hpp"
#include "../encoding.hpp"
#include "../ga/sga.hpp"
#include "../grasp/grasp.hpp"
#include "pop_based_restrict_list.hpp"

#define NUM_GENERATIONS_TO_TURNOVER 10
#define NUM_ENCODINGS_TO_CREATE_WITH_GRASP 20

namespace metaheuristics {

class HybridAlgorithm {
 public:
  HybridAlgorithm(SGA* sga, GRASP* grasp, const size_t gensUntilTurnover, ConvergenceStorageEvalContinue* continuator,
                  eoEvalFuncCounter<Chromosome>* evalFuncCounter)
      : sga(sga), grasp(grasp), gensUntilTurnover(gensUntilTurnover), cont(continuator), evalFuncCounter(evalFuncCounter) {
    this->popBasedRestrictListPtr = static_cast<PopBasedRestrictList*>(&grasp->getRestrictList());
  }

  void operator()(eoPop<Chromosome>& population) {
    apply<Chromosome>(*evalFuncCounter, population);
    eoPop<Chromosome> offspring;
    popBasedRestrictListPtr->setPopulation(&offspring);

    do {
      for (size_t g = 0; g < gensUntilTurnover; g++) {
        sga->evolvePopulation(population, offspring);
      }
      spdlog::info("Turnover after {} evals: starting GRASP", evalFuncCounter->value());
      for (size_t n = 0; n < NUM_ENCODINGS_TO_CREATE_WITH_GRASP; n++) {
        Encoding createdEncoding = grasp->buildEncoding();
        population.it_worse_element()->swap(createdEncoding);
      }
    } while ((*cont)(population));
    controllers::ExecutionData::getInstance()->setConvergence(cont->getConvergence());
  }

 private:
  SGA* sga;
  GRASP* grasp;
  const size_t gensUntilTurnover;
  ConvergenceStorageEvalContinue* cont;
  eoEvalFuncCounter<Chromosome>* evalFuncCounter;
  PopBasedRestrictList* popBasedRestrictListPtr;
};

}  // namespace metaheuristics

#endif  // METAHEURISTICS_HYBRID_ALGORITHM_HPP_
