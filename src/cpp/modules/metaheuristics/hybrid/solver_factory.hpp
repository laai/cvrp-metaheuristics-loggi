#ifndef METAHEURISTICS_HYBRID_SOLVER_FACTORY_HPP_
#define METAHEURISTICS_HYBRID_SOLVER_FACTORY_HPP_

#include <loggibud/cvrp_instance.h>

#include <controllers/config.hpp>
#include <controllers/solver.hpp>
#include <controllers/solver_factory.hpp>

namespace metaheuristics {

class HybridSolverFactory : public controllers::SolverFactory {
 public:
  HybridSolverFactory(controllers::TomlConfig* cfg) : config(cfg) {}

  controllers::Solver* createSolver(loggibud::CVRPInstance const* instance, const cvrp::graphs::Matrix* matrix);

 private:
  controllers::TomlConfig* config;
};

}  // namespace metaheuristics

#endif  // METAHEURISTICS_HYBRID_SOLVER_FACTORY_HPP_
