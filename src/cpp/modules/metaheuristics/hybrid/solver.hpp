#ifndef METAHEURISTICS_HYBRID_SOLVER_HPP_
#define METAHEURISTICS_HYBRID_SOLVER_HPP_

#include <controllers/solver.hpp>
#include <cvrp/decoding_service.hpp>
#include <memory>

#include "algorithm.hpp"

namespace metaheuristics {

class HybridSolver : public controllers::Solver {
 public:
  HybridSolver(size_t, HybridAlgorithm, cvrp::DecodingService*);
  loggibud::CVRPSolution solve(loggibud::CVRPInstance const*);
  const char* getName();

 private:
  const size_t popSize;
  HybridAlgorithm alg;
  cvrp::DecodingService* decoder;
};

}  // namespace metaheuristics

#endif  // METAHEURISTICS_HYBRID_SOLVER_HPP_
