#include "solver_factory.hpp"

#include <paradiseo/eo/eoOrderXover.h>
#include <paradiseo/eo/eoSwapMutation.h>

#include <cvrp/decoding_service.hpp>

#include "../ga/custom_tournament_select.hpp"
#include "../ga/sga.hpp"
#include "../grasp/grasp.hpp"
#include "algorithm.hpp"
#include "pop_based_restrict_list.hpp"
#include "solver.hpp"

using namespace metaheuristics;
using namespace cvrp;

controllers::Solver *HybridSolverFactory::createSolver(loggibud::CVRPInstance const *instance,
                                                       const cvrp::graphs::Matrix *_) {
  const uint64_t numberOfEvaluations = this->config->getULong("number-of-evaluations");
  const uint64_t tournamentSize = this->config->getTournamentSize();
  const double crossingRate = this->config->getCrossingRate();
  const double mutationRate = this->config->getMutationRate();
  const uint64_t priorityListSize = this->config->getPriorityListSize();
  const uint64_t neighborhoodSearchSize = this->config->getNeighborhoodSearchSize();

  const uint64_t gensUntilTurnover = this->config->getGensUntilTurnover();

  DecodingService *decodingService = new DecodingService((loggibud::CVRPInstance &)*instance);
  RoutingService *routingService = RoutingService::loadFromEnv();
  eoEvalFunc<Encoding> *evaluationFunction = new CVRPEvalFunc(std::shared_ptr<DecodingService>(decodingService),
                                                              std::shared_ptr<RoutingService>(routingService));

  auto *evalFuncCounter = new eoEvalFuncCounter<Encoding>(*evaluationFunction);
  auto *convergencialEvalContinue = new ConvergenceStorageEvalContinue(evalFuncCounter, numberOfEvaluations);

  eoSelectOne<Encoding> *selection = new CustomTournamentSelect(tournamentSize);
  eoQuadOp<Encoding> *crossover = new eoOrderXover<Encoding>();
  eoMonOp<Encoding> *mutation = new eoSwapMutation<Encoding>();
  SGA *geneticAlgorithm = new SGA(*selection, *crossover, crossingRate, *mutation, mutationRate, *evalFuncCounter,
                                  *convergencialEvalContinue);

  IRestrictList<size_t> *restrictList = new PopBasedRestrictList(priorityListSize, routingService);
  GRASP *grasp = new GRASP((loggibud::CVRPInstance &)*instance, *restrictList, *evalFuncCounter,
                           (ConvergenceStorageEvalContinue &)*convergencialEvalContinue, neighborhoodSearchSize);

  HybridAlgorithm algorithm =
      HybridAlgorithm(geneticAlgorithm, grasp, gensUntilTurnover, convergencialEvalContinue, evalFuncCounter);

  return new HybridSolver(this->config->getPopSize(), algorithm, decodingService);
}