#ifndef METAHEURISTICS_ENCODING_HPP_
#define METAHEURISTICS_ENCODING_HPP_

#include <paradiseo/eo/eoInit.h>
#include <paradiseo/eo/eoPop.h>
#include <paradiseo/eo/eoVector.h>

#include <memory>

namespace metaheuristics {

/**
 * @brief Representação de soluções usando permutação de números inteiros.
 * Possui um fitness e uma flag que diz se a codificação foi avaliada ou não.
 * @author Ronaldd Pinho <ronaldppinho@gmail.com>
 */
typedef eoVector<double, int32_t> Encoding;
typedef eoPop<Encoding> Population;
typedef Encoding Chromosome;

}  // namespace metaheuristics

#endif  // METAHEURISTICS_ENCODING_HPP_
