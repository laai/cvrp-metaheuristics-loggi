#ifndef METAHEURISTICS_STOP_CRITERIA_HPP_
#define METAHEURISTICS_STOP_CRITERIA_HPP_

#include <paradiseo/eo/eoEvalContinue.h>

#include "encoding.hpp"

namespace metaheuristics {

/**
 * @brief This class is a stop criteria controller based on maximum number of evaluations
 * that also stores the convergence each time which the stop criteria is checked.
 */
class ConvergenceStorageEvalContinue : public eoEvalContinue<Encoding> {
 public:
  ConvergenceStorageEvalContinue(eoEvalFuncCounter<Encoding>* evalFuncCounter,
                                 unsigned long totalEvaluations)
      : eoEvalContinue<Encoding>((eoEvalFuncCounter<Encoding>&)*evalFuncCounter, totalEvaluations),
        evalFuncCounter(evalFuncCounter) {}

  bool operator()(const eoPop<Encoding>& pop) override {
    fitnessConvergence.push_back((double)(1 / pop.best_element().fitness()));
    return evalFuncCounter->value() < this->totalEvaluations();
  }

  bool operator()(const Encoding& enc) {
    fitnessConvergence.push_back((double)(1 / enc.fitness()));
    return evalFuncCounter->value() < this->totalEvaluations();
  }

  std::vector<double>& getConvergence() { return fitnessConvergence; }

  eoEvalFuncCounter<Encoding>& getEvalFuncCounter() { return *evalFuncCounter; }

 private:
  std::vector<double> fitnessConvergence;
  eoEvalFuncCounter<Encoding>* evalFuncCounter;
};

}  // namespace metaheuristics

#endif  // METAHEURISTICS_STOP_CRITERIA_HPP_
