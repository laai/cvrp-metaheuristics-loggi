#ifndef METAHEURISTICS_HAMMING_HPP_
#define METAHEURISTICS_HAMMING_HPP_

#include <vector>

namespace metaheuristics {

template <typename T>
T hamming_distance(std::vector<T>&, std::vector<T>&);

}

#endif  // METAHEURISTICS_HAMMING_HPP_
