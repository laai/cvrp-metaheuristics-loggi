#include "encoding_builder.hpp"

#include <gtest/gtest.h>

#include <algorithm>

using namespace metaheuristics;

TEST(EncodingBuilderTest, shouldCreateAnEncodingWith5Elements) {
  Encoding encoding = EncodingBuilder(5).build();
  EXPECT_EQ(encoding.size(), 5);
}

TEST(EncodingBuilderTest, shouldCreateAnEncodingWithValuesLesserThan10) {
  Encoding encoding = EncodingBuilder(10).build();
  bool allIsLesserThan10 =
      std::all_of(encoding.cbegin(), encoding.cend(), [](uint32_t value) { return value < 10; });
  EXPECT_TRUE(allIsLesserThan10);
}

TEST(EncodingBuilderTest, shouldCreateAPopulationWith10Elements) {
  eoPop<Encoding> population = EncodingBuilder(3).buildPopulation(10);
  EXPECT_EQ(population.size(), 10);
}