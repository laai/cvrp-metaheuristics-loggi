#ifndef METAHEURISTICS_BUILDER_HPP_
#define METAHEURISTICS_BUILDER_HPP_

namespace metaheuristics {

template <class T>
class Builder {
 public:
  virtual T build() = 0;
};
}  // namespace metaheuristics

#endif  // METAHEURISTICS_BUILDER_HPP_
