#ifndef CVRP_METAHEURISTICS_ENCODING_BUILDER_HPP_
#define CVRP_METAHEURISTICS_ENCODING_BUILDER_HPP_

#include <paradiseo/eo/eoPop.h>

#include <random>

#include "builder.hpp"
#include "encoding.hpp"

namespace metaheuristics {

class EncodingBuilder : public Builder<Encoding> {
 public:
  EncodingBuilder(const size_t encodingLength) : length(encodingLength) {
    generator = std::mt19937(rd());
  }

  Population buildPopulation(const size_t popSize);
  Encoding build();

 private:
  unsigned long length;
  std::random_device rd;
  std::mt19937 generator;

  void initEncoding(Encoding& encoding);
};

}  // namespace metaheuristics

#endif  // CVRP_METAHEURISTICS_ENCODING_BUILDER_HPP_
