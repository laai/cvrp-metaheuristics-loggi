#ifndef METAHEURISTICS_GA_CROSSOVERS_ALTERNATING_EDGES_CROSSOVER_HPP_
#define METAHEURISTICS_GA_CROSSOVERS_ALTERNATING_EDGES_CROSSOVER_HPP_

#include <paradiseo/eo/eoOp.h>

#include <cvrp/adjacency_matrix.hpp>
#include <metaheuristics/encoding.hpp>
#include <random>

namespace metaheuristics::ga_crossovers {

class AlternatingEdgesCrossover : public eoQuadOp<metaheuristics::Encoding> {
 public:
  AlternatingEdgesCrossover();
  bool operator()(metaheuristics::Encoding &, metaheuristics::Encoding &);

 private:
  std::random_device rd;
  std::mt19937 engine;
};

}  // namespace metaheuristics::ga_crossovers

#endif  // METAHEURISTICS_GA_CROSSOVERS_ALTERNATING_EDGES_CROSSOVER_HPP_
