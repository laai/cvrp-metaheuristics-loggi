#ifndef METAHEURISTICS_GA_CROSSOVERS_PRECEDENCE_PRESERVATIVE_CROSSOVER_HPP_
#define METAHEURISTICS_GA_CROSSOVERS_PRECEDENCE_PRESERVATIVE_CROSSOVER_HPP_

#include <paradiseo/eo/eoOp.h>

#include <random>

#include "../../encoding.hpp"

namespace metaheuristics::ga_crossovers {

class PrecedencePreservativeCrossover : public eoQuadOp<Encoding> {
 public:
  PrecedencePreservativeCrossover();
  bool operator()(Encoding &, Encoding &);

 private:
  std::random_device rd;
  std::mt19937 generator;
};

}  // namespace metaheuristics::ga_crossovers

#endif  // METAHEURISTICS_GA_CROSSOVERS_PRECEDENCE_PRESERVATIVE_CROSSOVER_HPP_
