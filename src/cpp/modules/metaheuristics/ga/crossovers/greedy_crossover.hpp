#ifndef METAHEURISTICS_GA_CROSSOVERS_GREEDY_CROSSOVER_HPP_
#define METAHEURISTICS_GA_CROSSOVERS_GREEDY_CROSSOVER_HPP_

#include <paradiseo/eo/eoOp.h>

#include <cvrp/adjacency_matrix.hpp>
#include <metaheuristics/encoding.hpp>
#include <random>

namespace metaheuristics::ga_crossovers {

class GreedyCrossover : public eoQuadOp<metaheuristics::Encoding> {
 public:
  GreedyCrossover(const cvrp::graphs::Matrix*);
  Encoding generateChild(metaheuristics::Encoding&, metaheuristics::Encoding&);
  bool operator()(metaheuristics::Encoding&, metaheuristics::Encoding&);

 private:
  std::random_device rd;
  std::mt19937 engine;
  const cvrp::graphs::Matrix* matrix;
};

}  // namespace metaheuristics::ga_crossovers

#endif  // METAHEURISTICS_GA_CROSSOVERS_GREEDY_CROSSOVER_HPP_
