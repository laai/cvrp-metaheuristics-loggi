#ifndef METAHEURISTICS_GA_CROSSOVERS_GREEDY_ORDER_CROSSOVER_HPP_
#define METAHEURISTICS_GA_CROSSOVERS_GREEDY_ORDER_CROSSOVER_HPP_

#include <paradiseo/eo/eoOp.h>

#include <cvrp/adjacency_matrix.hpp>
#include <metaheuristics/encoding.hpp>
#include <random>

namespace metaheuristics::ga_crossovers {

class GreedyOrderCrossover : public eoQuadOp<metaheuristics::Encoding> {
 public:
  GreedyOrderCrossover(const cvrp::graphs::Matrix *, const std::vector<int> &, uint64_t);
  int generatePoint(int, int);
  Encoding generateChild(Encoding &parent1, Encoding &parent2, int point1, int point2);
  bool operator()(metaheuristics::Encoding &, metaheuristics::Encoding &);

 private:
  std::random_device rd;
  std::mt19937 engine;
  const cvrp::graphs::Matrix *matrix;
  const std::vector<int> &deliveryWeights;
  uint64_t sortingBufferSize;
};

}  // namespace metaheuristics::ga_crossovers

#endif  // METAHEURISTICS_GA_CROSSOVERS_GREEDY_ORDER_CROSSOVER_HPP_
