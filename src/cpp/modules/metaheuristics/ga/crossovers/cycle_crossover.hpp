#ifndef METAHEURISTICS_GA_CROSSOVERS_CYCLE_CROSSOVERS_HPP_
#define METAHEURISTICS_GA_CROSSOVERS_CYCLE_CROSSOVERS_HPP_

#include <paradiseo/eo/eoOp.h>

#include "../../encoding.hpp"

using metaheuristics::Encoding;

namespace metaheuristics::ga_crossovers {

class CycleCrossover : public eoQuadOp<Encoding> {
 public:
  CycleCrossover();
  bool operator()(Encoding&, Encoding&);
};

}  // namespace metaheuristics::ga_crossovers

#endif  // METAHEURISTICS_GA_CROSSOVERS_CYCLE_CROSSOVERS_HPP_
