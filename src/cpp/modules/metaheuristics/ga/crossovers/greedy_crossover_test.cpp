#include "greedy_crossover.hpp"

#include <gtest/gtest.h>

#include "../../encoding_builder.hpp"

using namespace metaheuristics;
using namespace metaheuristics::ga_crossovers;

TEST(GreedyCrossover, keepEncodingsAsPermutations) {
  EncodingBuilder encodingBuilder(10);
  Encoding encoding1 = encodingBuilder.build();
  Encoding encoding2 = encodingBuilder.build();

  cvrp::graphs::Matrix matrix(10);

  for (int i = 0; i <= 8; i++) {
    for (int j = 1; j <= 8; j++) {
      matrix.set(i, j, (double)i / j);
    }
  }
  GreedyCrossover crossover(&matrix);

  crossover(encoding1, encoding2);

  // Convert to set to verify if all elements are different
  std::set<uint32_t> encoding1AsSet(encoding1.begin(), encoding1.end());
  std::set<uint32_t> encoding2AsSet(encoding2.begin(), encoding2.end());

  EXPECT_EQ(10, encoding1AsSet.size());
  EXPECT_EQ(10, encoding2AsSet.size());
  EXPECT_EQ(encoding1.size(), encoding1AsSet.size());
  EXPECT_EQ(encoding2.size(), encoding2AsSet.size());
}