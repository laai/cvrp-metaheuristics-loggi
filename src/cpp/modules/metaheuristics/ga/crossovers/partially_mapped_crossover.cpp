#include "partially_mapped_crossover.hpp"

using namespace metaheuristics::ga_crossovers;
using metaheuristics::Encoding;

/**
 * Generate a random integer as uint32_t between a given range
 */
uint32_t PartiallyMappedCrossover::generatePoint(uint32_t low, uint32_t high) {
  std::uniform_int_distribution<uint32_t> randDistribution(low, high);
  return randDistribution(this->generator);
}

/**
 * Default constructor
 */
PartiallyMappedCrossover::PartiallyMappedCrossover() { this->generator = std::mt19937(this->rd()); }

/**
 * Main operation of crossover two encodings
 */
bool PartiallyMappedCrossover::operator()(Encoding& parent1, Encoding& parent2) {
  const uint32_t size = parent1.size();
  const uint32_t point1 = this->generatePoint(0, size - 3);
  const uint32_t point2 = this->generatePoint(point1 + 1, size - 1);
  const int32_t maxv = std::numeric_limits<int32_t>::max();

  Encoding child1(size, maxv);
  Encoding child2(size, maxv);

  for (uint32_t i = point1; i <= point2; ++i) {
    child1[i] = parent1[i];
    child2[i] = parent2[i];
  }

  std::unordered_map<uint32_t, uint32_t> mappingParent2ToParent1;
  std::unordered_map<uint32_t, uint32_t> mappingParent1ToParent2;

  for (uint32_t i = point1; i <= point2; ++i) {
    mappingParent2ToParent1[parent2[i]] = parent1[i];
    mappingParent1ToParent2[parent1[i]] = parent2[i];
  }

  for (uint32_t i = 0; i < size; ++i) {
    if (i < point1 || i > point2) {
      uint32_t allele = parent2[i];
      while (child1[i] == maxv) {
        if (mappingParent1ToParent2.find(allele) == mappingParent1ToParent2.end()) {
          child1[i] = allele;
        } else {
          allele = mappingParent1ToParent2[allele];
        }
      }
    }

    if (i < point1 || i > point2) {
      uint32_t allele = parent1[i];
      while (child2[i] == maxv) {
        if (mappingParent2ToParent1.find(allele) == mappingParent2ToParent1.end()) {
          child2[i] = allele;
        } else {
          allele = mappingParent2ToParent1[allele];
        }
      }
    }
  }
  parent1 = child1;
  parent2 = child2;

  return true;
}
