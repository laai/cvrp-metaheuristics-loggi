#ifndef METAHEURISTICS_GA_CROSSOVERS_MOD_ORDER_CROSSOVER_HPP_
#define METAHEURISTICS_GA_CROSSOVERS_MOD_ORDER_CROSSOVER_HPP_

#include <paradiseo/eo/eoOp.h>

#include <random>

#include "../../encoding.hpp"

namespace metaheuristics::ga_crossovers {

class ModifiedOrderCrossover : public eoQuadOp<Encoding> {
 public:
  ModifiedOrderCrossover();

  int32_t generateCutPoint(int32_t low, int32_t high);

  bool operator()(Encoding &, Encoding &);

 private:
  using vector_pair = std::pair<std::vector<int32_t>, std::vector<int32_t> >;
  std::random_device rd;
  std::mt19937 generator;

  vector_pair breakEncoding(Encoding &enc, int32_t cutPoint);
};

}  // namespace metaheuristics::ga_crossovers

#endif