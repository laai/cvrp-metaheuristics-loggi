#include "linear_order_crossover.hpp"

#include <gtest/gtest.h>

#include "../../encoding_builder.hpp"

using namespace metaheuristics;
using namespace metaheuristics::ga_crossovers;

TEST(LinearOrderCrossover, keepEncodingsAsPermutations) {
  LinearOrderCrossover crossover;
  EncodingBuilder encodingBuilder(8);
  Encoding encoding1 = encodingBuilder.build();
  Encoding encoding2 = encodingBuilder.build();

  crossover(encoding1, encoding2);

  // Convert to set to verify if all elements are different
  std::set<uint32_t> encoding1AsSet(encoding1.begin(), encoding1.end());
  std::set<uint32_t> encoding2AsSet(encoding2.begin(), encoding2.end());

  EXPECT_EQ(8, encoding1AsSet.size());
  EXPECT_EQ(8, encoding2AsSet.size());
  EXPECT_EQ(encoding1.size(), encoding1AsSet.size());
  EXPECT_EQ(encoding2.size(), encoding2AsSet.size());
}