#include "linear_order_crossover.hpp"

#include <algorithm>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <vector>

metaheuristics::ga_crossovers::LinearOrderCrossover::LinearOrderCrossover() {
  this->generator = std::mt19937(this->rd());
}

bool metaheuristics::ga_crossovers::LinearOrderCrossover::operator()(metaheuristics::Encoding& parent1,
                                                                     metaheuristics::Encoding& parent2) {
  int chromosomeLength = parent1.size();
  std::uniform_int_distribution<int> randDist(0, chromosomeLength-1);

  metaheuristics::Encoding child1(chromosomeLength), child2(chromosomeLength);
  int point1 = randDist(this->generator);
  int point2 = randDist(this->generator);

  if (point1 > point2) {
    std::swap(point1, point2);
  }

  for (int i = point1; i <= point2; i++) {
    child1[i] = parent1[i];
    child2[i] = parent2[i];
  }

  int index1 = 0;
  int index2 = 0;

  for (int i = 0; i < chromosomeLength; i++) {
    if (index1 == point1) {
      index1 = point2 + 1;
    }
    if (std::find(child1.begin(), child1.end(), parent2[index2]) == child1.end()) {
      child1[index1] = parent2[index2];
      index1++;
    }
    index2++;
  }

  index1 = 0;
  index2 = 0;

  for (int i = 0; i < chromosomeLength; i++) {
    if (index2 == point1) {
      index2 = point2 + 1;
    }
    if (std::find(child2.begin(), child2.end(), parent1[index1]) == child2.end()) {
      child2[index2] = parent1[index1];
      index2++;
    }
    index1++;
  }

  parent1.swap(child1);
  parent2.swap(child2);

  return true;
}