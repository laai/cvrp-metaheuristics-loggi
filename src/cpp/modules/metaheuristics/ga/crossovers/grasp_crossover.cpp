#include "grasp_crossover.hpp"

#include <iostream>

#include "greedy_order_crossover.hpp"

using namespace metaheuristics;

using std::pair;
using std::vector;

ga_crossovers::GraspCrossover::GraspCrossover(const cvrp::graphs::Matrix *matrix) : matrix(matrix) {
  engine = std::mt19937(rd());
}

int ga_crossovers::GraspCrossover::generatePoint(int low, int high) {
  std::uniform_int_distribution<int> dist(low, high - 1);
  return dist(engine);
}

Encoding ga_crossovers::GraspCrossover::generateChild(Encoding &parent1, Encoding &parent2, int point1, int point2) {
  Encoding child(parent1.size(), -1);
  vector<bool> childContains(parent1.size(), false);
  vector<int> notInChild;

  for (int i = 0; i < (int)parent1.size(); i++) {
    if (i >= point1 && i < point2) {
      child[i] = parent1[i];
      childContains[parent1[i]] = true;
    } else {
      notInChild.push_back(parent1[i]);
    }
  }

  int currentGeneInChild = child.at(point2 - 1);

  for (size_t i = 0; i < notInChild.size(); i++) {
    // Sort all missing genes by distance to last gene in child
    std::sort(notInChild.begin() + i, notInChild.end(), [&](int a, int b) {
      double distance_a = matrix->get(currentGeneInChild, a);
      double distance_b = matrix->get(currentGeneInChild, b);
      return distance_a < distance_b;
    });

    // Randomly select one gene between N first
    std::uniform_int_distribution<int> rand(0, (int)(notInChild.size() / 2));
    int randIndex = rand(engine);
    int aux = notInChild.at(i);
    notInChild[i] = notInChild.at(randIndex);
    notInChild[randIndex] = aux;

    currentGeneInChild = notInChild[i];
  }

  size_t index = point2;
  for (int gene : notInChild) {
    child.at(index++) = gene;
    if (index == child.size()) {
      index = 0;
    }
  }

  return child;
}

bool ga_crossovers::GraspCrossover::operator()(Encoding &parent1, Encoding &parent2) {
  size_t chromosomeLength = parent1.size();
  int point2 = generatePoint(1, chromosomeLength);
  int point1 = generatePoint(0, point2);

  Encoding child1 = generateChild(parent1, parent2, point1, point2);
  Encoding child2 = generateChild(parent2, parent1, point1, point2);

  parent1.swap(child1);
  parent2.swap(child2);

  return true;
}
