#ifndef METAHEURISTICS_GA_CROSSOVERS_PARTIALLY_MAPPED_CROSSOVER_HPP_
#define METAHEURISTICS_GA_CROSSOVERS_PARTIALLY_MAPPED_CROSSOVER_HPP_

#include <paradiseo/eo/eoOp.h>

#include <random>

#include "../../encoding.hpp"

namespace metaheuristics::ga_crossovers {

class PartiallyMappedCrossover : public eoQuadOp<Encoding> {
 public:
  PartiallyMappedCrossover();
  uint32_t generatePoint(uint32_t low, uint32_t high);
  bool operator()(Encoding&, Encoding&);

 private:
  std::random_device rd;
  std::mt19937 generator;
};

}  // namespace metaheuristics::ga_crossovers

#endif  // METAHEURISTICS_GA_CROSSOVERS_PARTIALLY_MAPPED_CROSSOVER_HPP_
