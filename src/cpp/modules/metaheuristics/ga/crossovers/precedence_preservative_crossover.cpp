#include "precedence_preservative_crossover.hpp"

#include <algorithm>

using namespace metaheuristics;
using namespace metaheuristics::ga_crossovers;

PrecedencePreservativeCrossover::PrecedencePreservativeCrossover() { this->generator = std::mt19937(this->rd()); }

Encoding generateChild(std::vector<bool> &mask, Encoding &parent1, Encoding &parent2) {
  const size_t chromosomeLength = parent1.size();
  Encoding child;
  std::vector<bool> childContains(chromosomeLength, false);

  int index1 = 0, index2 = 0;

  for (size_t i = 0; i < chromosomeLength; i++) {
    if (mask[i]) {
      while (childContains[parent2[index2]]) {
        index2++;
      }
      child.push_back(parent2[index2]);
      childContains[parent2[index2]] = true;
    } else {
      while (childContains[parent1[index1]]) {
        index1++;
      }
      child.push_back(parent1[index1]);
      childContains[parent1[index1]] = true;
    }
  }

  return child;
}

bool PrecedencePreservativeCrossover::operator()(Encoding &parent1, Encoding &parent2) {
  size_t chromosomeSize = parent1.size();
  std::uniform_int_distribution<int> dist;

  std::vector<bool> mask(chromosomeSize, false);
  for (size_t i = 0; i < chromosomeSize; i++) {
    mask[i] = dist(this->generator) % 2;
  }
  Encoding child1 = generateChild(mask, parent1, parent2);
  Encoding child2 = generateChild(mask, parent2, parent1);

  parent1.swap(child1);
  parent2.swap(child2);

  return true;
}