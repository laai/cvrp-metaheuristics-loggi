#include "greedy_order_crossover.hpp"

#include <spdlog/spdlog.h>
using namespace metaheuristics::ga_crossovers;
using namespace metaheuristics;

using std::pair;
using std::vector;

GreedyOrderCrossover::GreedyOrderCrossover(const cvrp::graphs::Matrix *matrix, const std::vector<int> &deliveryWeights,
                                           uint64_t sortingBufferSize)
    : matrix(matrix), deliveryWeights(deliveryWeights), sortingBufferSize(sortingBufferSize) {
  engine = std::mt19937(rd());
}

int GreedyOrderCrossover::generatePoint(int low, int high) {
  std::uniform_int_distribution<int> dist(low, high - 1);
  return dist(engine);
}

Encoding GreedyOrderCrossover::generateChild(Encoding &parent1, Encoding &parent2, int point1, int point2) {
  const size_t chromosomeLength = parent1.size();
  Encoding child(chromosomeLength, -1);
  vector<bool> inChild(chromosomeLength, false);
  vector<int> notInChild;

  for (int i = point1; i < point2; i++) {
    child[i] = parent1[i];
    inChild[parent1[i]] = true;
  }

  for (size_t i = 0; i < parent2.size(); i++) {
    if (!inChild[parent2[i]]) {
      notInChild.push_back(parent2[i]);
    }
  }

  // const size_t N = (size_t)notInChild.size() * this->sortRate;  // Sorting rate (%). Default is 10%
  int currentGeneInChild = child.at(point2 - 1);

  for (size_t i = 0; i < notInChild.size(); i++) {
    size_t k = i + this->sortingBufferSize < notInChild.size() ? i + this->sortingBufferSize : notInChild.size() - 1;

    std::sort(notInChild.begin() + i, notInChild.begin() + k, [&](int a, int b) {
      double distance_a = matrix->get(currentGeneInChild, a);
      double distance_b = matrix->get(currentGeneInChild, b);
      return distance_a < distance_b;
    });

    currentGeneInChild = notInChild[i];
  }

  size_t index = point2;
  for (int gene : notInChild) {
    child.at(index++) = gene;
    if (index == child.size()) {
      index = 0;
    }
  }

  return child;
}

bool GreedyOrderCrossover::operator()(Encoding &parent1, Encoding &parent2) {
  size_t chromosomeLength = parent1.size();
  int point2 = generatePoint(1, chromosomeLength);
  int point1 = generatePoint(0, point2);

  Encoding child1 = generateChild(parent1, parent2, point1, point2);
  Encoding child2 = generateChild(parent2, parent1, point1, point2);

  parent1.swap(child1);
  parent2.swap(child2);

  return true;
}
