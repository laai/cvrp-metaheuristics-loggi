#include "mod_order_crossover.hpp"

#include <algorithm>
#include <map>

using namespace metaheuristics::ga_crossovers;

ModifiedOrderCrossover::ModifiedOrderCrossover() { this->generator = std::mt19937(this->rd()); }

int32_t ModifiedOrderCrossover::generateCutPoint(int32_t low, int32_t high) {
  std::uniform_int_distribution<int32_t> randDist(low, high);
  return randDist(this->generator);
}

ModifiedOrderCrossover::vector_pair ModifiedOrderCrossover::breakEncoding(metaheuristics::Encoding &enc,
                                                                          int32_t cutPoint) {
  vector_pair vecPair;
  vecPair.first = std::vector<int32_t>(enc.begin(), enc.begin() + cutPoint, enc.get_allocator());
  vecPair.second = std::vector<int32_t>(enc.begin() + cutPoint, enc.end(), enc.get_allocator());
  return vecPair;
}

bool vectorContains(std::vector<int32_t> &vec, int32_t value) {
  return std::find(vec.begin(), vec.end(), value) != vec.end();
}

bool ModifiedOrderCrossover::operator()(metaheuristics::Encoding &first_encoding,
                                        metaheuristics::Encoding &second_encoding) {
  size_t maximumCutPoint = first_encoding.size() - 1;
  int32_t cutPoint = this->generateCutPoint(1, maximumCutPoint);

  vector_pair parent1 = breakEncoding(first_encoding, cutPoint);
  vector_pair parent2 = breakEncoding(second_encoding, cutPoint);

  size_t j1 = 0, j2 = 0;
  for (size_t i = 0; i < first_encoding.size(); i++) {
    if (!vectorContains(parent2.first, first_encoding[i])) {
      first_encoding[i] = parent2.second[j1];
      j1 += 1;
    }
    if (!vectorContains(parent1.first, second_encoding[i])) {
      second_encoding[i] = parent1.second[j2];
      j2 += 1;
    }
  }
  return true;
}