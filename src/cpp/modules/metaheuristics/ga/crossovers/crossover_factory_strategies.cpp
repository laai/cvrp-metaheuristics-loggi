#include "crossover_factory_strategies.hpp"

#include <paradiseo/eo/eoOrderXover.h>
#include <paradiseo/eo/eoPartiallyMappedXover.h>
#include <spdlog/spdlog.h>

#include "alternating_edges_crossover.hpp"
#include "cycle_crossover.hpp"
#include "grasp_crossover.hpp"
#include "greedy_crossover.hpp"
#include "greedy_order_crossover.hpp"
#include "linear_order_crossover.hpp"
#include "mod_order_crossover.hpp"
#include "precedence_preservative_crossover.hpp"

using namespace metaheuristics::ga_crossovers;
using std::vector;

const char *CX = "CX";
const char *OX = "OX";
const char *MOX = "MOX";
const char *LOX = "LOX";
const char *PMX = "PMX";
const char *PPX = "PPX";
const char *AEX = "AEX";
const char *GX = "GX";
const char *GOX = "GOX";
const char *GRASPBX = "GRASPBX";

vector<const char *> metaheuristics::ga_crossovers::getAllCrossoverIDs() {
  return vector<const char *>({CX, OX, MOX, LOX, PMX, PPX, AEX, GX, GOX, GRASPBX});
}

///-----------------------------------------------------------------------------------------------------
/// Cycle Crossover
///-----------------------------------------------------------------------------------------------------

CycleCrossoverFactory::CycleCrossoverFactory() {}

bool CycleCrossoverFactory::shouldExecute(std::string key) { return key.compare(CX) == 0; }

eoQuadOp<metaheuristics::Encoding> *CycleCrossoverFactory::createCrossover() {
  spdlog::info("Using Cycle Crossover operator (CX)");
  return new CycleCrossover();
}

///-----------------------------------------------------------------------------------------------------
/// Order Crossover
///-----------------------------------------------------------------------------------------------------

OrderCrossoverFactory::OrderCrossoverFactory() {}

bool OrderCrossoverFactory::shouldExecute(std::string key) { return key.compare(OX) == 0; }

eoQuadOp<metaheuristics::Encoding> *OrderCrossoverFactory::createCrossover() {
  spdlog::info("Using the Order Crossover (OX)");
  return new eoOrderXover<metaheuristics::Encoding>();
}

///-----------------------------------------------------------------------------------------------------
/// Modified Order Crossover
///-----------------------------------------------------------------------------------------------------

ModifiedOrderCrossoverFactory::ModifiedOrderCrossoverFactory() {}

bool ModifiedOrderCrossoverFactory::shouldExecute(std::string key) { return key.compare(MOX) == 0; }

eoQuadOp<metaheuristics::Encoding> *ModifiedOrderCrossoverFactory::createCrossover() {
  spdlog::info("Using the Modified Order Crossover (MOX)");
  return new ModifiedOrderCrossover();
}

///-----------------------------------------------------------------------------------------------------
/// Linear Order Crossover
///-----------------------------------------------------------------------------------------------------

LinearOrderCrossoverFactory::LinearOrderCrossoverFactory() {}

bool LinearOrderCrossoverFactory::shouldExecute(std::string key) { return key.compare(LOX) == 0; }

eoQuadOp<metaheuristics::Encoding> *LinearOrderCrossoverFactory::createCrossover() {
  spdlog::info("Using the Linear Order Crossover (LOX)");
  return new LinearOrderCrossover();
}

///-----------------------------------------------------------------------------------------------------
/// Partially Mapped Crossover
///-----------------------------------------------------------------------------------------------------

PartiallyMappedCrossoverFactory::PartiallyMappedCrossoverFactory() {}

bool PartiallyMappedCrossoverFactory::shouldExecute(std::string key) { return key.compare(PMX) == 0; }

eoQuadOp<metaheuristics::Encoding> *PartiallyMappedCrossoverFactory::createCrossover() {
  spdlog::info("Using the Partially Mapped Crossover (PMX)");
  return new eoPartiallyMappedXover<metaheuristics::Encoding>();
}

///-----------------------------------------------------------------------------------------------------
/// Precedence Preservative Crossover
///-----------------------------------------------------------------------------------------------------

PrecedencePreservativeCrossoverFactory::PrecedencePreservativeCrossoverFactory() {}

bool PrecedencePreservativeCrossoverFactory::shouldExecute(std::string key) { return key.compare(PPX) == 0; }

eoQuadOp<metaheuristics::Encoding> *PrecedencePreservativeCrossoverFactory::createCrossover() {
  spdlog::info("Using the Precedence Preservative Crossover (PPX)");
  return new PrecedencePreservativeCrossover();
}

///-----------------------------------------------------------------------------------------------------
/// Alternating Edges Crossover
///-----------------------------------------------------------------------------------------------------

AlternatingEdgesCrossoverFactory::AlternatingEdgesCrossoverFactory() {}

bool AlternatingEdgesCrossoverFactory::shouldExecute(std::string key) { return key.compare(AEX) == 0; }

eoQuadOp<metaheuristics::Encoding> *AlternatingEdgesCrossoverFactory::createCrossover() {
  spdlog::info("Using the Alternating Edges Crossover (AEX)");
  return new AlternatingEdgesCrossover();
}

///-----------------------------------------------------------------------------------------------------
/// Greedy Crossover
///-----------------------------------------------------------------------------------------------------

GreedyCrossoverFactory::GreedyCrossoverFactory(const cvrp::graphs::Matrix *matrix) : matrix(matrix) {}

bool GreedyCrossoverFactory::shouldExecute(std::string key) { return key.compare(GX) == 0; }

eoQuadOp<metaheuristics::Encoding> *GreedyCrossoverFactory::createCrossover() {
  spdlog::info("Using the Greedy Crossover (GX)");
  return new GreedyCrossover(this->matrix);
}

///-----------------------------------------------------------------------------------------------------
/// My Proposed Crossover for CVRP
///-----------------------------------------------------------------------------------------------------

GreedyOrderCrossoverFactory::GreedyOrderCrossoverFactory(const cvrp::graphs::Matrix *matrix,
                                                         const std::vector<int> &deliveryWeights, uint64_t sortingBufferSize)
    : matrix(matrix), deliveryWeights(deliveryWeights), sortingBufferSize(sortingBufferSize) {}

bool GreedyOrderCrossoverFactory::shouldExecute(std::string key) { return key.compare(GOX) == 0; }

eoQuadOp<metaheuristics::Encoding> *GreedyOrderCrossoverFactory::createCrossover() {
  spdlog::info("Using the R.Pinho's Greedy Order Crossover (GOX) with buffer size = {}", this->sortingBufferSize);
  return new GreedyOrderCrossover(this->matrix, this->deliveryWeights, this->sortingBufferSize);
}

///-----------------------------------------------------------------------------------------------------
/// GRASP General/Restrict List Based Crossover
///-----------------------------------------------------------------------------------------------------

GraspBasedCrossoverFactory::GraspBasedCrossoverFactory(const cvrp::graphs::Matrix *matrix) : matrix(matrix) {}

bool GraspBasedCrossoverFactory::shouldExecute(std::string key) { return key.compare(GRASPBX) == 0; }

eoQuadOp<metaheuristics::Encoding> *GraspBasedCrossoverFactory::createCrossover() {
  spdlog::info("Using the GRASP List Based Crossover (GRASPBX)");
  return new GraspCrossover(this->matrix);
}
