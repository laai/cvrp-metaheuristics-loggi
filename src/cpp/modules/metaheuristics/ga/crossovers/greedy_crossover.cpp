#include "greedy_crossover.hpp"

using namespace metaheuristics::ga_crossovers;
using metaheuristics::Encoding;

inline size_t get_index_of(std::vector<int>& vec, int value) {
  for (size_t i = 0; i < vec.size(); i++) {
    if (vec[i] == value) {
      return i;
    }
  }
  return vec.size();
}

GreedyCrossover::GreedyCrossover(const cvrp::graphs::Matrix* matrix) : matrix(matrix) { engine = std::mt19937(rd()); }

Encoding GreedyCrossover::generateChild(Encoding& parent1, Encoding& parent2) {
  const size_t chromosomeLength = parent1.size();
  Encoding child;
  std::vector<bool> inChild(chromosomeLength, false);

  std::uniform_int_distribution<int> uniform(0, chromosomeLength - 1);
  int firstGene = uniform(this->engine);
  child.push_back(firstGene);
  inChild[firstGene] = true;

  while (child.size() < chromosomeLength) {
    int currentGene = *(child.end() - 1);
    int index1 = get_index_of(parent1, currentGene);
    int index2 = get_index_of(parent2, currentGene);

    std::vector<int> neighbors = {
      parent1[(index1 - 1) % chromosomeLength],
      parent1[(index1 + 1) % chromosomeLength],
      parent2[(index2 - 1) % chromosomeLength],
      parent2[(index2 + 1) % chromosomeLength]
    };
    std::sort(neighbors.begin(), neighbors.end(), [&](int a, int b) {
      double distance_a = this->matrix->get(currentGene, a);
      double distance_b = this->matrix->get(currentGene, b);
      return distance_a < distance_b;
    });

    size_t prevSize = child.size();
    for (size_t i = 0; i < neighbors.size(); i++) {
      if (!inChild[neighbors[i]]) {
        child.push_back(neighbors[i]);
        inChild[neighbors[i]] = true;
        currentGene = neighbors[i];
        break;
      }
    }
    if (child.size() == prevSize) {
      int randGene = uniform(this->engine);
      while (inChild[randGene]) {
        randGene = uniform(this->engine);
      }
      child.push_back(randGene);
      inChild[randGene] = true;
      currentGene = randGene;
    }
  }

  return child;
}

bool GreedyCrossover::operator()(Encoding& parent1, Encoding& parent2) { 
  Encoding child1 = this->generateChild(parent1, parent2);
  Encoding child2 = this->generateChild(parent1, parent2);
  parent1.swap(child1);
  parent2.swap(child2);
  return true;
}