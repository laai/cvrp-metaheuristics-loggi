#include "grasp_crossover.hpp"

#include <gtest/gtest.h>

#include "../../encoding_builder.hpp"

using namespace metaheuristics;
using namespace metaheuristics::ga_crossovers;

TEST(GraspCrossover, keepEncodingsAsPermutations) {
  EncodingBuilder encodingBuilder(8);
  Encoding encoding1 = encodingBuilder.build();
  Encoding encoding2 = encodingBuilder.build();

  cvrp::graphs::Matrix matrix(8);

  for (int i = 0; i <= 8; i++) {
    for (int j = 1; j <= 8; j++) {
      matrix.set(i, j, (double)i / j);
    }
  }

  GraspCrossover crossover(&matrix);
  crossover(encoding1, encoding2);

  // Convert to set to verify if all elements are different
  std::set<int32_t> encoding1AsSet(encoding1.begin(), encoding1.end());
  std::set<int32_t> encoding2AsSet(encoding2.begin(), encoding2.end());

  EXPECT_EQ(8, encoding1AsSet.size());
  EXPECT_EQ(8, encoding2AsSet.size());
  EXPECT_EQ(encoding1.size(), encoding1AsSet.size());
  EXPECT_EQ(encoding2.size(), encoding2AsSet.size());
}