#include "alternating_edges_crossover.hpp"

using namespace metaheuristics::ga_crossovers;
using metaheuristics::Encoding;

#include <cstdlib>
#include <ctime>
#include <iostream>
#include <vector>

inline size_t get_index_of(std::vector<int>& vec, int value) {
  for (size_t i = 0; i < vec.size(); i++) {
    if (vec[i] == value) {
      return i;
    }
  }
  return vec.size();
}

AlternatingEdgesCrossover::AlternatingEdgesCrossover() { this->engine = std::mt19937(this->rd()); }

// Function to perform Alternating Edges Crossover (AEX)
bool AlternatingEdgesCrossover::operator()(Encoding& parent1, Encoding& parent2) {
  size_t chromosomeLength = parent1.size();

  Encoding child1(chromosomeLength, -1);
  Encoding child2(chromosomeLength, -2);

  std::vector<bool> inChild1(chromosomeLength, false);
  std::vector<bool> inChild2(chromosomeLength, false);

  child1[0] = parent1[0];
  child2[0] = parent2[0];
  inChild1[parent1[0]] = true;
  inChild2[parent2[0]] = true;

  std::uniform_int_distribution<int> uniform(0, chromosomeLength - 1);

  for (size_t i = 1; i < chromosomeLength; i++) {
    if (i % 2 == 1) {
      size_t index1 = get_index_of(parent1, child1[i - 1]);
      //   std::cout << "Índice de " << child1[i - 1] << " em p1 é " << index1 << "\t";

      if (!inChild1[parent1[(index1 + 1) % chromosomeLength]]) {
        child1[i] = parent1[(index1 + 1) % chromosomeLength];
        inChild1[parent1[(index1 + 1) % chromosomeLength]] = true;
        // std::cout << "c1 recebe " << parent1[(index1 + 1) % chromosomeLength] << " na posição " << i << std::endl;
      } else {
        int rand = uniform(this->engine);
        while (inChild1[parent1[rand]]) {
          rand = uniform(this->engine);
        }
        child1[i] = parent1[rand];
        inChild1[parent1[rand]] = true;
        // std::cout << "c1 recebe " << parent1[rand] << " na posição " << i << " (random)" << std::endl;
      }

      size_t index2 = get_index_of(parent2, child2[i - 1]);
      //   std::cout << "Índice de " << child2[i - 1] << " em p2 é " << index2 << "\t";

      if (!inChild2[parent2[(index2 + 1) % chromosomeLength]]) {
        child2[i] = parent2[(index2 + 1) % chromosomeLength];
        inChild2[parent2[(index2 + 1) % chromosomeLength]] = true;
        inChild2[parent2[(index2 + 1) % chromosomeLength]];
        // std::cout << "c2 recebe " << parent2[(index2 + 1) % chromosomeLength] << " na posição " << i << std::endl;
      } else {
        int rand = uniform(this->engine);
        while (inChild2[parent2[rand]]) {
          rand = uniform(this->engine);
        }
        child2[i] = parent2[rand];
        inChild2[parent2[rand]] = true;
        inChild2[parent2[rand]] = true;
        // std::cout << "c2 recebe " << parent2[rand] << " na posição " << i << " (random)" << std::endl;
      }
    } else {
      size_t index1 = get_index_of(parent2, child1[i - 1]);
      //   std::cout << "Índice de " << child1[i - 1] << " em p2 é " << index1 << "\t";

      if (!inChild1[parent2[(index1 + 1) % chromosomeLength]]) {
        child1[i] = parent2[(index1 + 1) % chromosomeLength];
        inChild1[parent2[(index1 + 1) % chromosomeLength]] = true;
        // std::cout << "c1 recebe " << parent2[(index1 + 1) % chromosomeLength] << " na posição " << i << std::endl;
      } else {
        int rand = uniform(this->engine);
        while (inChild1[parent2[rand]]) {
          rand = uniform(this->engine);
        }
        child1[i] = parent2[rand];
        inChild1[parent2[rand]] = true;
        // std::cout << "c1 recebe " << parent2[rand] << " na posição " << i << " (random)" << std::endl;
      }

      size_t index2 = get_index_of(parent1, child2[i - 1]);
      //   std::cout << "Índice de " << child2[i - 1] << " em p1 é " << index2 << "\t";
      if (!inChild2[parent1[(index2 + 1) % chromosomeLength]]) {
        child2[i] = parent1[(index2 + 1) % chromosomeLength];
        inChild2[parent1[(index2 + 1) % chromosomeLength]] = true;
        // std::cout << "c2 recebe " << parent1[(index2 + 1) % chromosomeLength] << " na posição " << i << std::endl;
      } else {
        int rand = uniform(this->engine);
        while (inChild2[parent1[rand]]) {
          rand = uniform(this->engine);
        }
        child2[i] = parent1[rand];
        inChild2[parent1[rand]] = true;
        // std::cout << "c2 recebe " << parent1[rand] << " na posição " << i << " (random)" << std::endl;
      }
    }
  }

  parent1.swap(child1);
  parent2.swap(child2);

  return true;
}

// AEX in Python:
//
// def alternating_edges_crossover(parent1, parent2) :
//     n = parent1.length
//     child1 = Chromosome(np.full(n,-1), np.random.randint(1,100), n)
//     child2 = Chromosome(np.full(n,-1), np.random.randint(1,100), n)

//     child1.genes[0] = parent1.genes[0]
//     child2.genes[0] = parent2.genes[0]

//     for i in range(1,n) :
//         if i%2 == 1:
//             if not parent1.genes[(list(parent1.genes).index(child1.genes[i-1]) + 1)%n] in child1.genes :
//                 child1.genes[i] = parent1.genes[(list(parent1.genes).index(child1.genes[i-1]) + 1)%n]
//             else:
//                 rand = np.random.randint(0,n)
//                 while parent1.genes[rand] in child1.genes :
//                     rand = np.random.randint(0,n)
//                 child1.genes[i] = parent1.genes[rand]

//             if not parent2.genes[(list(parent2.genes).index(child2.genes[i-1]) + 1)%n] in child2.genes :
//                 child2.genes[i] = parent2.genes[(list(parent2.genes).index(child2.genes[i-1]) + 1)%n]
//             else:
//                 rand = np.random.randint(0,n)
//                 while parent2.genes[rand] in child2.genes :
//                     rand = np.random.randint(0,n)
//                 child2.genes[i] = parent2.genes[rand]

//         else:
//             if not parent2.genes[(list(parent2.genes).index(child1.genes[i-1]) + 1)%n] in child1.genes :
//                 child1.genes[i] = parent2.genes[(list(parent2.genes).index(child1.genes[i-1]) + 1)%n]
//             else:
//                 rand = np.random.randint(0,n)
//                 while parent2.genes[rand] in child1.genes :
//                     rand = np.random.randint(0,n)
//                 child1.genes[i] = parent2.genes[rand]

//             if not parent1.genes[(list(parent1.genes).index(child2.genes[i-1]) + 1)%n] in child2.genes :
//                 child2.genes[i] = parent1.genes[(list(parent1.genes).index(child2.genes[i-1]) + 1)%n]
//             else:
//                 rand = np.random.randint(0,n)
//                 while parent1.genes[rand] in child2.genes :
//                     rand = np.random.randint(0,n)
//                 child2.genes[i] = parent1.genes[rand]

//     return child1, child2