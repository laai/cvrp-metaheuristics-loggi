#include "cycle_crossover.hpp"

#include <unordered_set>

using namespace metaheuristics::ga_crossovers;

CycleCrossover::CycleCrossover() {}

bool CycleCrossover::operator()(Encoding& parent1, Encoding& parent2) {
  uint32_t size = parent1.size();
  Encoding child1(size, -1);
  Encoding child2(size, -1);
  std::unordered_set<uint32_t> visited;

  int32_t cycleStart = parent1[0];

  do {
    uint32_t index = std::distance(parent1.begin(), std::find(parent1.begin(), parent1.end(), cycleStart));
    visited.insert(cycleStart);

    child1[index] = parent1[index];
    child2[index] = parent2[index];
    cycleStart = parent2[index];
  } while (cycleStart != parent1[0]);

  for (uint32_t i = 0; i < size; ++i) {
    if (child1[i] == -1) {
      child1[i] = parent2[i];
    }
    if (child2[i] == -1) {
      child2[i] = parent1[i];
    }
  }

  // Update the parents with the child values
  parent1 = child1;
  parent2 = child2;

  return true;
}
