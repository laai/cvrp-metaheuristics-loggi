#ifndef METAHEURISTICS_GA_CROSSOVERS_CROSSOVER_FACTORY_STRATEGIES_HPP_
#define METAHEURISTICS_GA_CROSSOVERS_CROSSOVER_FACTORY_STRATEGIES_HPP_

#include <paradiseo/eo/eoOp.h>

#include <cvrp/adjacency_matrix.hpp>
#include <metaheuristics/encoding.hpp>
#include <string>

namespace metaheuristics::ga_crossovers {

std::vector<const char*> getAllCrossoverIDs();

class CrossoverFactory {
 public:
  virtual bool shouldExecute(std::string key) = 0;
  virtual eoQuadOp<Encoding>* createCrossover() = 0;
};

class CycleCrossoverFactory : public CrossoverFactory {
 public:
  CycleCrossoverFactory();
  bool shouldExecute(std::string key);
  eoQuadOp<Encoding>* createCrossover();
};

class OrderCrossoverFactory : public CrossoverFactory {
 public:
  OrderCrossoverFactory();
  bool shouldExecute(std::string key);
  eoQuadOp<Encoding>* createCrossover();
};

class ModifiedOrderCrossoverFactory : public CrossoverFactory {
 public:
  ModifiedOrderCrossoverFactory();
  bool shouldExecute(std::string key);
  eoQuadOp<Encoding>* createCrossover();
};

class LinearOrderCrossoverFactory : public CrossoverFactory {
 public:
  LinearOrderCrossoverFactory();
  bool shouldExecute(std::string key);
  eoQuadOp<Encoding>* createCrossover();
};

class PartiallyMappedCrossoverFactory : public CrossoverFactory {
 public:
  PartiallyMappedCrossoverFactory();
  bool shouldExecute(std::string key);
  eoQuadOp<Encoding>* createCrossover();
};

class PrecedencePreservativeCrossoverFactory : public CrossoverFactory {
 public:
  PrecedencePreservativeCrossoverFactory();
  bool shouldExecute(std::string key);
  eoQuadOp<Encoding>* createCrossover();
};

class AlternatingEdgesCrossoverFactory : public CrossoverFactory {
 public:
  AlternatingEdgesCrossoverFactory();
  bool shouldExecute(std::string key);
  eoQuadOp<Encoding>* createCrossover();
};

class GreedyCrossoverFactory : public CrossoverFactory {
 public:
  GreedyCrossoverFactory(const cvrp::graphs::Matrix*);
  bool shouldExecute(std::string key);
  eoQuadOp<Encoding>* createCrossover();

 private:
  const cvrp::graphs::Matrix* matrix;
};

//

class GreedyOrderCrossoverFactory : public CrossoverFactory {
 public:
  GreedyOrderCrossoverFactory(const cvrp::graphs::Matrix*, const std::vector<int>&, uint64_t);
  bool shouldExecute(std::string key);
  eoQuadOp<Encoding>* createCrossover();

 private:
  const cvrp::graphs::Matrix* matrix;
  const std::vector<int>& deliveryWeights;
  uint64_t sortingBufferSize;
};

class GraspBasedCrossoverFactory : public CrossoverFactory {
 public:
  GraspBasedCrossoverFactory(const cvrp::graphs::Matrix*);
  bool shouldExecute(std::string key);
  eoQuadOp<Encoding>* createCrossover();

 private:
  const cvrp::graphs::Matrix* matrix;
};

}  // namespace metaheuristics::ga_crossovers

#endif  // METAHEURISTICS_GA_CROSSOVERS_CROSSOVER_FACTORY_STRATEGIES_HPP_
