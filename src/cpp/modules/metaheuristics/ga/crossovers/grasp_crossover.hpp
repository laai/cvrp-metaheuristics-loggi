#ifndef METAHEURISTICS_GA_CROSSOVERS_GRASP_CROSSOVER_HPP_
#define METAHEURISTICS_GA_CROSSOVERS_GRASP_CROSSOVER_HPP_

#include <paradiseo/eo/eoOp.h>

#include <cvrp/adjacency_matrix.hpp>
#include <metaheuristics/encoding.hpp>
#include <random>

namespace metaheuristics::ga_crossovers {

class GraspCrossover : public eoQuadOp<Encoding> {
 public:
  GraspCrossover(const cvrp::graphs::Matrix *);
  int generatePoint(int, int);
  Encoding generateChild(Encoding &parent1, Encoding &parent2, int point1, int point2);
  bool operator()(metaheuristics::Encoding &, metaheuristics::Encoding &);

 private:
  std::random_device rd;
  std::mt19937 engine;
  const cvrp::graphs::Matrix *matrix;
};

}  // namespace metaheuristics::ga_crossovers

#endif  // METAHEURISTICS_GA_CROSSOVERS_GRASP_CROSSOVER_HPP_