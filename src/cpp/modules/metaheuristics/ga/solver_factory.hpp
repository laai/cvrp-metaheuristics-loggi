#ifndef METAHEURISTICS_GA_SOLVER_FACTORY_HPP_
#define METAHEURISTICS_GA_SOLVER_FACTORY_HPP_

#include <controllers/solver_factory.hpp>

namespace metaheuristics {

class SGASolverFactory : public controllers::SolverFactory {
 public:
  SGASolverFactory(controllers::TomlConfig* config, std::string crossover)
      : config(config), crossoverKey(crossover){}

  controllers::Solver* createSolver(loggibud::CVRPInstance const*, const cvrp::graphs::Matrix*);

 private:
  controllers::TomlConfig* config;
  std::string crossoverKey;
};

}  // namespace metaheuristics

#endif  // METAHEURISTICS_GA_SOLVER_FACTORY_HPP_
