#include "solver.hpp"

#include <controllers/execution_data.hpp>

#include "../encoding_builder.hpp"

using namespace metaheuristics;

SGASolver::SGASolver(size_t popSize, SGA sga, cvrp::DecodingService* decoder)
    : popSize(popSize), sga(sga), decoder(decoder) {}

const char* SGASolver::getName() { return "genetic_algorithm"; }

loggibud::CVRPSolution SGASolver::solve(loggibud::CVRPInstance const* instance) {
  Population population = EncodingBuilder(instance->deliveries.size()).buildPopulation(this->popSize);
  this->sga(population);
  Encoding best = population.best_element();
  controllers::ExecutionData::getInstance()->setTotalDistance(1 / best.fitness());
  controllers::ExecutionData::getInstance()->setSolutionIndices(best);
  return this->decoder->decode(best);
}
