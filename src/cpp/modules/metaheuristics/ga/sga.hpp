#ifndef CVRP_METAHEURISTICS_GA_SGA_HPP_
#define CVRP_METAHEURISTICS_GA_SGA_HPP_

#include <paradiseo/eo/eoEvalContinue.h>
#include <paradiseo/eo/eoEvalFuncCounter.h>
#include <paradiseo/eo/eoPop.h>
#include <paradiseo/eo/eoSGA.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/spdlog.h>

#include <controllers/execution_data.hpp>

#include "../convergence_storage_eval_continue.hpp"
#include "../encoding.hpp"

namespace metaheuristics {

class SGA {
 public:
  explicit SGA(eoSelectOne<Chromosome>& selection, eoQuadOp<Chromosome>& crossoverOp,
               float crossoverRate, eoMonOp<Chromosome>& mutationOp, float mutationRate,
               eoEvalFuncCounter<Chromosome>& eval, ConvergenceStorageEvalContinue& continuator)
      : select(selection),
        cross(crossoverOp),
        crossoverRate(crossoverRate),
        mutate(mutationOp),
        mutationRate(mutationRate),
        evalFuncCounter(eval),
        cont(continuator) {}

  void operator()(eoPop<Chromosome>& population) {
    eoPop<Chromosome> offspring;
    apply<Chromosome>(evalFuncCounter, population);
    do {
      evolvePopulation(population, offspring);
    } while (cont(population));
    controllers::ExecutionData::getInstance()->setConvergence(cont.getConvergence());
  }

  void evolvePopulation(eoPop<Chromosome>& population, eoPop<Chromosome>& offspring) {
    select(population, offspring);
    applyCrossover(offspring, population.size());
    applyMutation(offspring);
    population.swap(offspring);
    apply<Chromosome>(evalFuncCounter, population);
  }

  void applyCrossover(eoPop<Chromosome>& pop, size_t numChroms) {
    for (unsigned i = 0; i < numChroms / 2; i++) {
      if (rng.flip(crossoverRate)) {
        if (cross(pop[2 * i], pop[2 * i + 1])) {
          pop[2 * i].invalidate();
          pop[2 * i + 1].invalidate();
        }
      }
    }
  }

  void applyMutation(eoPop<Chromosome>& pop) {
    for (unsigned i = 0; i < pop.size(); i++) {
      if (rng.flip(mutationRate)) {
        if (mutate(pop[i])) pop[i].invalidate();
      }
    }
  }

 private:
  eoSelectPerc<Chromosome> select;
  eoQuadOp<Chromosome>& cross;
  float crossoverRate;
  eoMonOp<Chromosome>& mutate;
  float mutationRate;
  eoEvalFuncCounter<Chromosome>& evalFuncCounter;
  ConvergenceStorageEvalContinue& cont;
};

}  // namespace metaheuristics

#endif  // CVRP_METAHEURISTICS_GA_SGA_HPP_
