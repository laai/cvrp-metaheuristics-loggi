#ifndef CVRP_METAHEURISTICS_GA_SOLVER_HPP_
#define CVRP_METAHEURISTICS_GA_SOLVER_HPP_

#include <controllers/solver.hpp>
#include <cvrp/decoding_service.hpp>

#include "sga.hpp"

namespace metaheuristics {

class SGASolver : public controllers::Solver {
 public:
  SGASolver(size_t, SGA, cvrp::DecodingService*);
  loggibud::CVRPSolution solve(loggibud::CVRPInstance const*);
  const char* getName();

 private:
  const size_t popSize;
  SGA sga;
  cvrp::DecodingService* decoder;
};

}  // namespace metaheuristics

#endif  // CVRP_METAHEURISTICS_GA_SOLVER_HPP_
