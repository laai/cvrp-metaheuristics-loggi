#ifndef METAHEURISTICS_GA_CUSTOM_TOURNAMENT_SELECT_HPP_
#define METAHEURISTICS_GA_CUSTOM_TOURNAMENT_SELECT_HPP_

#include <paradiseo/eo/eoSelectOne.h>
#include <paradiseo/eo/utils/selectors.h>

#include "../encoding.hpp"

namespace metaheuristics {

class CustomTournamentSelect : public eoSelectOne<Chromosome> {
 public:
  CustomTournamentSelect(const size_t tourSize) : tourSize(tourSize) {}

  const Chromosome& operator()(const eoPop<Chromosome>& population) {
    return deterministic_tournament(population, tourSize);
  }

 private:
  const size_t tourSize;
};

}  // namespace metaheuristics

#endif  // METAHEURISTICS_GA_CUSTOM_TOURNAMENT_SELECT_HPP_
