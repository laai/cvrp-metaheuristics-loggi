#include "solver_factory.hpp"

#include <paradiseo/eo/eoOrderXover.h>
#include <paradiseo/eo/eoSwapMutation.h>

#include <cvrp/adjacency_matrix.hpp>
#include <cvrp/cvrp_eval_func.hpp>
#include <cvrp/decoding_service.hpp>

#include "../convergence_storage_eval_continue.hpp"
#include "crossovers/crossover_factory_strategies.hpp"
#include "crossovers/cycle_crossover.hpp"
#include "crossovers/grasp_crossover.hpp"
#include "crossovers/greedy_order_crossover.hpp"
#include "crossovers/linear_order_crossover.hpp"
#include "crossovers/mod_order_crossover.hpp"
#include "crossovers/partially_mapped_crossover.hpp"
#include "crossovers/precedence_preservative_crossover.hpp"
#include "custom_tournament_select.hpp"
#include "sga.hpp"
#include "solver.hpp"

using namespace metaheuristics;
using namespace metaheuristics::ga_crossovers;
using namespace cvrp;

controllers::Solver *SGASolverFactory::createSolver(loggibud::CVRPInstance const *instance,
                                                    const cvrp::graphs::Matrix *matrix) {
  const uint64_t numberOfEvaluations = this->config->getULong("number-of-evaluations");
  const uint64_t tournamentSize = this->config->getTournamentSize();
  const double crossingRate = this->config->getCrossingRate();
  const double mutationRate = this->config->getMutationRate();
  const uint64_t sortingBufferSize = this->config->getGOXSortingBufferSize();

  std::vector<int> deliveryWeights;
  for (auto& delivery : instance->deliveries) {
    deliveryWeights.push_back(delivery.size);
  }

  DecodingService *decodingService = new DecodingService((loggibud::CVRPInstance &)*instance);

  eoEvalFunc<Encoding> *evaluationFunction = new CachedDistancesEvalFunc(
      std::shared_ptr<DecodingService>(decodingService), (loggibud::CVRPInstance &)*instance, matrix);

  auto *evalFuncCounter = new eoEvalFuncCounter<Encoding>(*evaluationFunction);
  auto *convergencialEvalContinue = new ConvergenceStorageEvalContinue(evalFuncCounter, numberOfEvaluations);

  std::vector<CrossoverFactory *> xoverFactories = {new CycleCrossoverFactory(),
                                                    new OrderCrossoverFactory(),
                                                    new ModifiedOrderCrossoverFactory(),
                                                    new LinearOrderCrossoverFactory(),
                                                    new PartiallyMappedCrossoverFactory(),
                                                    new PrecedencePreservativeCrossoverFactory(),
                                                    new AlternatingEdgesCrossoverFactory(),
                                                    new GreedyCrossoverFactory(matrix),
                                                    new GreedyOrderCrossoverFactory(matrix, deliveryWeights, sortingBufferSize),
                                                    new GraspBasedCrossoverFactory(matrix)};

  eoSelectOne<Encoding> *selection = new CustomTournamentSelect(tournamentSize);
  eoQuadOp<Encoding> *crossover;
  eoMonOp<Encoding> *mutation = new eoSwapMutation<Encoding>();

  controllers::ExecutionData::getInstance()->setCrossover(this->crossoverKey);
  controllers::ExecutionData::getInstance()->setAlgorithmProperties(fmt::format("S={}", sortingBufferSize));
  for (CrossoverFactory *xoverFactory : xoverFactories) {
    if (xoverFactory->shouldExecute(this->crossoverKey)) {
      crossover = xoverFactory->createCrossover();
    }
  }

  if (crossover == nullptr) {
    throw std::runtime_error(fmt::format("Unknown crossover operator: {}", this->crossoverKey));
  }

  SGA geneticAlgorithm(*selection, *crossover, crossingRate, *mutation, mutationRate, *evalFuncCounter,
                       *convergencialEvalContinue);

  return new SGASolver(this->config->getPopSize(), geneticAlgorithm, decodingService);
}