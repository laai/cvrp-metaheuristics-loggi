#include "hamming.hpp"

#include <cassert>

template <typename T>
T hamming_distance(std::vector<T>& word_a, std::vector<T>& word_b) {
  assert(word_a.size() == word_b.size());

  T sum = 0;
  for (size_t i = 0; i < word_a.size(); i++) {
    sum += (int) word_a[i] != word_b[i];
  }
  return sum;
}