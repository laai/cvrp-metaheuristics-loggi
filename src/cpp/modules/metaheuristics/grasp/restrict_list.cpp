#include "restrict_list.hpp"

using namespace metaheuristics;
using namespace loggibud;

void RestrictList::clear() { this->list.clear(); }

void RestrictList::populate(const Encoding &partialSolution, Point &point, CVRPInstance &problem,
                            set<size_t> indicesToIgnore = {}) {
  auto distancesMap = getDistancesToAllPoints(point, problem, indicesToIgnore);
  auto distancesVec = vector<Item>(distancesMap.begin(), distancesMap.end());
  std::sort(distancesVec.begin(), distancesVec.end(),
            [](Item &a, Item &b) { return a.second < b.second; });
  auto N = distancesVec.size() < this->size_ ? distancesVec.size() : size_;
  this->list = vector<Item>(distancesVec.begin(), distancesVec.begin() + N);
}

size_t RestrictList::choiceOne() {
  std::uniform_int_distribution<size_t> dist(0, this->list.size() - 1);
  size_t randomIndex = dist(mtGenerator);
  return this->list[randomIndex].first;
}

unordered_map<size_t, double> RestrictList::getDistancesToAllPoints(
    Point &point, CVRPInstance &problem, set<size_t> indicesToIgnore = {}) {
  using loggibud::Delivery;
  auto i = uint32_t(0);
  std::unordered_map<size_t, double> distances;
  for (Delivery &d : problem.deliveries) {
    if (indicesToIgnore.find(i) != indicesToIgnore.end()) {
      i++;
      continue;
    }
    double distance = this->routingService->distanceBetween(point, d.point);
    std::pair<size_t, double> p((size_t)i, distance);
    distances.insert(p);
    i++;
  }
  return distances;
}