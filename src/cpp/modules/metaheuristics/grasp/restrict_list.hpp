#ifndef METAHEURISTICS_GRASP_RESTRICT_LIST_HPP_
#define METAHEURISTICS_GRASP_RESTRICT_LIST_HPP_

#include <loggibud/cvrp_instance.h>

#include <algorithm>
#include <cvrp/routing_service.hpp>
#include <memory>
#include <random>
#include <set>
#include <unordered_map>
#include <vector>

#include "../encoding.hpp"
using loggibud::CVRPInstance;
using loggibud::Point;
using std::set;
using std::unordered_map;
using std::vector;

namespace metaheuristics {

template <typename ElemT>
class IRestrictList {
 public:
  virtual void clear() = 0;
  virtual void populate(const Encoding &, Point &, CVRPInstance &, set<ElemT>) = 0;
  virtual ElemT choiceOne() = 0;
  virtual void setCurrentValueInSolution(ElemT) = 0;
  virtual ElemT getCurrentValueInSolution() = 0;
};

/**
 * Manipula todos os métodos e responsabilidades relativas à lista de
 * prioridades para a construção de uma solução do grasp.
 */
class RestrictList : public IRestrictList<size_t> {
 public:
  using Item = std::pair<size_t, double>;

  RestrictList(const size_t size, cvrp::RoutingService *routingService)
      : size_(size), list(0), mtGenerator(rd()), routingService(routingService) {
    this->list.reserve(size);
  }

  void clear();
  void populate(const Encoding &, Point &, CVRPInstance &, set<size_t>);
  size_t choiceOne();

  void setCurrentValueInSolution(size_t value) { this->currentValueInSolution = value; }
  size_t getCurrentValueInSolution() { return this->currentValueInSolution; }

 protected:
  size_t size_;
  size_t currentValueInSolution;
  std::vector<Item> list;
  std::random_device rd;
  std::mt19937_64 mtGenerator;
  cvrp::RoutingService *routingService;

  /**
   * Calculate the distances between the received point and all others delivery
   * points defined in the problem instance and, returns as a map when the key
   * is the index of the delivery and value is the distance to the param point.
   *
   * \param point reference point to calculate the distance to all others
   * \param problem instance of the CVRP problem (loggibud::CVRPInstance)
   * \param indicesToIgnore a set of delivery indices to ignore when calculating
   * the distances
   *
   * @returns an map<K, V> where K = integer index and V = distance (float)
   */
  unordered_map<size_t, double> getDistancesToAllPoints(Point &, CVRPInstance &, set<size_t>);
};

}  // namespace metaheuristics

#endif  // METAHEURISTICS_GRASP_RESTRICT_LIST_HPP_
