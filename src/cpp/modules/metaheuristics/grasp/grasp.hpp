#ifndef METAHEURISTICS_GRASP_GRASP_HPP_
#define METAHEURISTICS_GRASP_GRASP_HPP_

#include <paradiseo/eo/eoEvalFuncCounter.h>
#include <paradiseo/eo/eoFunctor.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/spdlog.h>

#include <controllers/execution_data.hpp>
#include <set>
#include <unordered_map>
#include <vector>

#include "../convergence_storage_eval_continue.hpp"
#include "../encoding.hpp"
#include "neighborhood_search.hpp"
#include "restrict_list.hpp"

using std::set;
using std::unordered_map;

namespace metaheuristics {

class GRASP : eoF<Encoding> {
 public:
  GRASP(loggibud::CVRPInstance &problem, IRestrictList<size_t> &restrictList,
        eoEvalFuncCounter<Encoding> &eval, ConvergenceStorageEvalContinue &evalContinue,
        const size_t neighborhoodSearchSize)
      : problem(problem),
        restrictList(restrictList),
        eval(eval),
        evalContinue(evalContinue),
        neighborhoodSearch(eval),
        neighborhoodSearchSize(neighborhoodSearchSize) {}

  Encoding operator()() override {
    Encoding finalSolution, solutionBuilt;
    do {
      solutionBuilt = buildEncoding();
      neighborhoodSearch(solutionBuilt, neighborhoodSearchSize);
      if (shouldUpdateSolution(finalSolution, solutionBuilt)) {
        finalSolution = solutionBuilt;
      }
    } while (evalContinue(finalSolution));
    controllers::ExecutionData::getInstance()->setConvergence(evalContinue.getConvergence());
    return finalSolution;
  }

  Encoding buildEncoding() {
    Encoding solutionInBuilding;
    size_t occupied = 0;
    loggibud::Point currentPoint = this->problem.origin;
    set<size_t> unavailableValues;

    while (solutionInBuilding.size() < this->problem.deliveries.size()) {
      if (!solutionInBuilding.empty()) {
        this->restrictList.setCurrentValueInSolution(
            solutionInBuilding.at(solutionInBuilding.size() - 1));
      }
      this->restrictList.populate(solutionInBuilding, currentPoint, problem, unavailableValues);
      auto chosenValue = this->restrictList.choiceOne();
      auto deliverySize = this->problem.deliveries[chosenValue].size;
      this->restrictList.clear();

      if (occupied + deliverySize > this->problem.vehicle_capacity) {
        resetDataForNextIteration(&occupied, &currentPoint);
        continue;
      }
      solutionInBuilding.push_back(chosenValue);
      unavailableValues.insert(chosenValue);
      occupied += deliverySize;
    }
    solutionInBuilding.invalidate();
    this->eval(solutionInBuilding);
    return solutionInBuilding;
  }

  IRestrictList<size_t> &getRestrictList() {
    return this->restrictList;
  }

 private:
  loggibud::CVRPInstance &problem;
  IRestrictList<size_t> &restrictList;
  eoEvalFuncCounter<Encoding> &eval;
  ConvergenceStorageEvalContinue &evalContinue;
  NeighborhoodSearch neighborhoodSearch;
  const size_t neighborhoodSearchSize;

  void resetDataForNextIteration(size_t *occupied, loggibud::Point *currentPoint) {
    *occupied = 0;
    *currentPoint = this->problem.origin;
  }

  bool shouldUpdateSolution(const Encoding &currentSolution, const Encoding &newSolutionCandidate) {
    return currentSolution.invalid() || newSolutionCandidate.fitness() > currentSolution.fitness();
  }
};

}  // namespace metaheuristics

#endif  // METAHEURISTICS_GRASP_GRASP_HPP_
