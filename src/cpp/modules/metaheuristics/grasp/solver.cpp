#include "solver.hpp"

#include <controllers/execution_data.hpp>

#include "../encoding.hpp"

using namespace metaheuristics;

GRASPSolver::GRASPSolver(GRASP grasp, cvrp::DecodingService* decoder)
    : grasp(grasp), decoder(decoder) {}

const char* GRASPSolver::getName() { return "grasp"; }

loggibud::CVRPSolution GRASPSolver::solve(loggibud::CVRPInstance const* _) {
  Encoding encoding = this->grasp();
  controllers::ExecutionData::getInstance()->setTotalDistance(1 / encoding.fitness());
  return this->decoder->decode(encoding);
}
