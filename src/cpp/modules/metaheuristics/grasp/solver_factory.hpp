#ifndef METAHEURISTICS_GRASP_SOLVER_FACTORY_HPP_
#define METAHEURISTICS_GRASP_SOLVER_FACTORY_HPP_

#include <controllers/config.hpp>
#include <controllers/solver_factory.hpp>

namespace metaheuristics {

class GRASPSolverFactory : public controllers::SolverFactory {
 public:
  GRASPSolverFactory(controllers::TomlConfig* cfg) : config(cfg) {}

  controllers::Solver* createSolver(loggibud::CVRPInstance const*, const cvrp::graphs::Matrix*);

 private:
  controllers::TomlConfig* config;
};

}  // namespace metaheuristics

#endif  // METAHEURISTICS_GRASP_SOLVER_FACTORY_HPP_
