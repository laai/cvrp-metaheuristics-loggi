#include "solver_factory.hpp"

#include <cvrp/cvrp_eval_func.hpp>

#include "grasp.hpp"
#include "solver.hpp"

using namespace metaheuristics;
using namespace cvrp;

controllers::Solver *GRASPSolverFactory::createSolver(loggibud::CVRPInstance const *instance,
                                                      const cvrp::graphs::Matrix *_) {
  const uint64_t numberOfEvaluations = this->config->getULong("number-of-evaluations");
  const uint64_t priorityListSize = this->config->getPriorityListSize();
  const uint64_t neighborhoodSearchSize = this->config->getNeighborhoodSearchSize();

  DecodingService *decodingService = new DecodingService((loggibud::CVRPInstance &)*instance);
  RoutingService *routingService = RoutingService::loadFromEnv();
  eoEvalFunc<Encoding> *evaluationFunction = new CVRPEvalFunc(std::shared_ptr<DecodingService>(decodingService),
                                                              std::shared_ptr<RoutingService>(routingService));

  auto *evalFuncCounter = new eoEvalFuncCounter<Encoding>(*evaluationFunction);
  auto *convergencialEvalContinue = new ConvergenceStorageEvalContinue(evalFuncCounter, numberOfEvaluations);

  IRestrictList<size_t> *restrictList = new RestrictList(priorityListSize, routingService);
  GRASP grasp((loggibud::CVRPInstance &)*instance, *restrictList, *evalFuncCounter, *convergencialEvalContinue,
              neighborhoodSearchSize);

  return new GRASPSolver(grasp, decodingService);
}