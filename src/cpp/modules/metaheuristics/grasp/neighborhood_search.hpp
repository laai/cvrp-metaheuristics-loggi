#ifndef METAHEURISTICS_GRASP_NEIGHBORHOOD_SEARCH_HPP_
#define METAHEURISTICS_GRASP_NEIGHBORHOOD_SEARCH_HPP_

#include <paradiseo/eo/eoEvalFunc.h>
#include <paradiseo/eo/eoSwapMutation.h>
#include <spdlog/spdlog.h>

#include <cvrp/cvrp_eval_func.hpp>

#include "../encoding.hpp"

namespace metaheuristics {

/**
 * Perform a simple Neighborhood Search creating N neighbors from a reference solution.
 */
class NeighborhoodSearch {
 public:
  NeighborhoodSearch(eoEvalFuncCounter<Encoding>& eval) : evalFuncCounter(eval) {}

  void operator()(Encoding &refSolution, const size_t neighborhoodSize) {
    metaheuristics::Population neighborhood = createNeighborhood(refSolution, neighborhoodSize);
    for (metaheuristics::Encoding &encoding : neighborhood) {
      if (encoding.fitness() > refSolution.fitness()) {
        refSolution = encoding;
      }
    }
  }

 private:
  eoSwapMutation<Encoding> mutation;
  eoEvalFuncCounter<Encoding>& evalFuncCounter;

  Population createNeighborhood(const Encoding &refSolution, const size_t size) {
    metaheuristics::Population neighborhood;
    for (size_t i = 0; i < size; i++) {
      metaheuristics::Encoding neighbor = refSolution;
      mutation(neighbor);
      neighbor.invalidate();
      evalFuncCounter.operator()(neighbor);
      neighborhood.push_back(neighbor);
    }
    return neighborhood;
  }
};

}  // namespace metaheuristics

#endif  // METAHEURISTICS_GRASP_NEIGHBORHOOD_SEARCH_HPP_
