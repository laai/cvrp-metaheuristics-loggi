#ifndef METAHEURISTICS_GRASP_SOLVER_HPP_
#define METAHEURISTICS_GRASP_SOLVER_HPP_

#include <controllers/solver.hpp>
#include <cvrp/decoding_service.hpp>

#include "grasp.hpp"

namespace metaheuristics {

class GRASPSolver : public controllers::Solver {
 public:
  GRASPSolver(GRASP, cvrp::DecodingService*);
  loggibud::CVRPSolution solve(loggibud::CVRPInstance const*);
  const char* getName();

 private:
  GRASP grasp;
  cvrp::DecodingService* decoder;
};

}  // namespace metaheuristics

#endif  // METAHEURISTICS_GRASP_SOLVER_HPP_
