#include "encoding_builder.hpp"

metaheuristics::Population metaheuristics::EncodingBuilder::buildPopulation(const size_t popSize) {
  eoPop<metaheuristics::Encoding> population;
  for (uint32_t i = 0; i < (uint32_t)popSize; i++) {
    population.push_back(this->build());
  }
  return population;
}

metaheuristics::Encoding metaheuristics::EncodingBuilder::build() {
  metaheuristics::Encoding encoding;
  initEncoding(encoding);
  return encoding;
}

void metaheuristics::EncodingBuilder::initEncoding(metaheuristics::Encoding& encoding) {
  encoding.clear();
  for (uint32_t v = 0; v < length; v++) {
    encoding.push_back(v);
  }
  std::shuffle(encoding.begin(), encoding.end(), generator);
}
