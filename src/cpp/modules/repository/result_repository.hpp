#ifndef REPOSITORY_RESULT_REPOSITORY_HPP_
#define REPOSITORY_RESULT_REPOSITORY_HPP_

#include <spdlog/spdlog.h>
#include <sqlite3pp.h>

#include "models.hpp"
#include "repository.hpp"

namespace repository {

class ResultRepository : public Repository<models::ExperimentResult> {
 public:
  ResultRepository(sqlite3pp::database*);
  void save(models::ExperimentResult&);

 private:
  sqlite3pp::database* db;

  void createTable();
};

}  // namespace repository

#endif  // REPOSITORY_RESULT_REPOSITORY_HPP_
