#include "result_repository.hpp"

repository::ResultRepository::ResultRepository(sqlite3pp::database* db) : db(db) {
  this->createTable();
  spdlog::info("Database table was created");
}

void repository::ResultRepository::createTable() {
  db->execute(
      "CREATE TABLE IF NOT EXISTS experiment_results (id integer primary key autoincrement, "
      "instance text not null, solution text not null, permutation_solution text, algorithm_name text not null, "
      "algorithm_properties text, crossover_op text, final_total_distance float not null, convergence text, "
      "elapsed_time_ms integer, instance_name GENERATED ALWAYS AS (JSON_EXTRACT(instance, "
      "\"$.name\")) VIRTUAL, instance_region GENERATED ALWAYS AS (JSON_EXTRACT(instance, "
      "\"$.region\")) VIRTUAL, instance_vehicle_capacity GENERATED ALWAYS AS "
      "(JSON_EXTRACT(instance, \"$.vehicle_capacity\")) VIRTUAL, instance_num_deliveries GENERATED "
      "ALWAYS AS (JSON_ARRAY_LENGTH(\"instance\", \"$.deliveries\")) VIRTUAL, "
      "solution_num_vehicles GENERATED ALWAYS AS (JSON_ARRAY_LENGTH(\"solution\", \"$.vehicles\")) "
      "VIRTUAL);");
}

void repository::ResultRepository::save(repository::models::ExperimentResult& r) {
  sqlite3pp::command cmd(
      *db,
      "INSERT INTO experiment_results (instance, solution, permutation_solution, algorithm_name, "
      "final_total_distance, convergence, elapsed_time_ms, crossover_op) VALUES (?, ?, ?, ?, ?, ?, ?, ?);");

  cmd.binder() << r.instance << r.solution << r.solutionIndices << r.algorithmName << r.finalTotalDistance
               << r.convergence << r.elapsedTimeMs << r.crossover;
  cmd.execute();
  spdlog::info("Insert into database with success");
}
