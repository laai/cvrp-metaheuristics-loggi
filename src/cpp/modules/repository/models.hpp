#ifndef REPOSITORY_MODELS_HPP_
#define REPOSITORY_MODELS_HPP_

#define DELIM ","

#include <loggibud/cvrp_instance.h>
#include <loggibud/cvrp_solution.h>

#include <chrono>
#include <iterator>
#include <metaheuristics/builder.hpp>
#include <sstream>
#include <string>
#include <vector>

namespace repository::models {

struct ExperimentResult {
 public:
  int id;
  std::string instance;
  std::string solution;
  std::string solutionIndices;
  std::string algorithmName;
  std::string algorithmProperties;
  float finalTotalDistance;
  std::string convergence;
  int elapsedTimeMs;
  std::string crossover;
};

}  // namespace repository::models

#endif  // REPOSITORY_MODELS_HPP_
