cmake_minimum_required(VERSION 3.14)

add_library(repository
  result_repository.cpp
)

target_link_libraries(repository sqlite3)
target_include_directories(repository PRIVATE ${CMAKE_PROJECT_DIR}/third_party)

# unit tests
enable_testing()
file(GLOB TEST_FILES *_test.cpp)
add_executable(test-repository ${TEST_FILES})
target_include_directories(test-repository
  PUBLIC
  ${googletest_SOURCE_DIR}/googletest/include
  ${googletest_SOURCE_DIR}/googlemock/include
)
target_link_libraries(test-repository PRIVATE repository GTest::gtest_main)
