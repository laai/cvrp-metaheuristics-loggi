#include "models.hpp"

#include <gtest/gtest.h>

using namespace repository::models;

TEST(ExperimentResultBuilderTest, shouldBuildAModelWithAlgorithm) {
  ExperimentResult r;
  r.algorithmName = "genetic-algorithm";
  EXPECT_STREQ("genetic-algorithm", r.algorithmName.c_str());
}

TEST(ExperimentResultBuilderTest, shouldBuildAModelWithAlgorithmProperties) {
  ExperimentResult r;
  r.algorithmProperties = "{\"tour_size\":4}";
  EXPECT_STREQ("{\"tour_size\":4}", r.algorithmProperties.c_str());
}

TEST(ExperimentResultBuilderTest, shouldBuildAModelWithDistance) {
  ExperimentResult r;
  r.finalTotalDistance = 5000;
  EXPECT_EQ(5000, r.finalTotalDistance);
}
