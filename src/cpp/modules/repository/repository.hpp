#ifndef REPOSITORY_REPOSITORY_HPP_
#define REPOSITORY_REPOSITORY_HPP_

namespace repository {

template <class E>
class Repository {
 public:
  virtual void save(E&) = 0;
};

}  // namespace repository

#endif  // REPOSITORY_REPOSITORY_HPP_
