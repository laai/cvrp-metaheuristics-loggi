#ifndef CVRP_EVAL_FUNC_HPP_
#define CVRP_EVAL_FUNC_HPP_

#include <paradiseo/eo/eoEvalFunc.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/spdlog.h>

#include <cvrp/adjacency_matrix.hpp>
#include <cvrp/decoding_service.hpp>
#include <cvrp/routing_service.hpp>
#include <metaheuristics/encoding.hpp>

namespace cvrp {

/**
 * @class CVRPEvalFunc
 * @brief Definição e lógica da função de avaliação do problema do roteamento de
 * veículos com restrição de capacidade.
 */
class CVRPEvalFunc : public eoEvalFunc<metaheuristics::Encoding> {
 public:
  CVRPEvalFunc(std::shared_ptr<cvrp::DecodingService> decodingService,
               std::shared_ptr<cvrp::RoutingService> routingService)
      : decoder(decodingService), router(routingService) {}

  void operator()(metaheuristics::Encoding &chrom) override;

 private:
  std::shared_ptr<cvrp::DecodingService> decoder;
  std::shared_ptr<cvrp::RoutingService> router;
};

/**
 * @class CachedDistancesEvalFunc
 * @brief Função de avaliação de soluções baseada nas distâncias entre os pontos (índices)
 * cacheadas em memória em uma estrutura de matriz de adjacências.
 */
class CachedDistancesEvalFunc : public eoEvalFunc<metaheuristics::Encoding> {
 public:
  CachedDistancesEvalFunc(std::shared_ptr<cvrp::DecodingService> decodingService, loggibud::CVRPInstance &cvrpInstance,
                          const graphs::Matrix *matrix)
      : decoder(decodingService), cvrpInstance(cvrpInstance), matrix(matrix) {}

  void operator()(metaheuristics::Encoding &chrom) override;

 private:
  std::shared_ptr<cvrp::DecodingService> decoder;
  loggibud::CVRPInstance &cvrpInstance;
  const graphs::Matrix *matrix;

  std::vector<std::vector<int32_t>> decodeRoutes(metaheuristics::Encoding &chrom);
};

}  // namespace cvrp

#endif  // CVRP_EVAL_FUNC_HPP_
