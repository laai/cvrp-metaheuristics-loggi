#include "adjacency_matrix.hpp"

#include <gtest/gtest.h>

TEST(MatrixTest, shouldreturnTheValueSettedInTheIndices) {
  cvrp::graphs::Matrix matrix(10);
  matrix.set(6, 2, 13.6);
  EXPECT_EQ(13.6, matrix.get(6, 2));
}