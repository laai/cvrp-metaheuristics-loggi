#include "adjacency_matrix.hpp"

#include <spdlog/spdlog.h>

cvrp::graphs::Matrix::Matrix(size_t len)
    : std::vector<std::vector<double>>(len + 1, std::vector<double>(len + 1, 0.0)), length(len) {}

void cvrp::graphs::Matrix::set(size_t i, size_t k, double value) {
  this->at(i).at(k) = value;
  this->at(k).at(i) = value;
}

double cvrp::graphs::Matrix::get(size_t i, size_t k) const { return this->at(i).at(k); }

cvrp::graphs::Matrix* cvrp::graphs::create_matrix(const loggibud::CVRPInstance& instance) {
  const size_t numDeliveries = instance.deliveries.size();
  auto* routingService = cvrp::RoutingService::loadFromEnv();

  spdlog::info("Loading distances into a matrix; [{} deliveries]", numDeliveries);

  cvrp::graphs::Matrix* m = new cvrp::graphs::Matrix(numDeliveries);

  for (size_t i = 0; i < numDeliveries; i++) {
    loggibud::Delivery currentDelivery = instance.deliveries.at(i);

    double originDist = routingService->distanceBetween(instance.origin, currentDelivery.point);
    m->set(numDeliveries, i, originDist);

    for (size_t j = 0; j < numDeliveries; j++) {
      if (i != j) {
        double dist = routingService->distanceBetween(currentDelivery.point, instance.deliveries.at(j).point);
        m->set(i, j, dist);
      }
    }
  }
  delete routingService;
  return m;
}