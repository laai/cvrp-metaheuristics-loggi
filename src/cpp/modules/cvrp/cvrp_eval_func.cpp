#include "cvrp_eval_func.hpp"

#include <spdlog/spdlog.h>

#include <algorithm>

/**
 * Calcula o somatório das distâncias das rotas de cada veículo na solução e
 * define o fitness da codificação como o inverso desse valor.
 */
void cvrp::CVRPEvalFunc::operator()(metaheuristics::Encoding &chrom) {
  auto totalDistance = double(0);
  auto decodedSolution = decoder->decode(chrom);

  for (auto &vehicle : decodedSolution.vehicles) {
    totalDistance += router->getVehicleRouteDistance(vehicle);
  }

  chrom.fitness(1 / totalDistance);
}

/**
 * Calcula o somatório das distâncias de cada rota baseando-se em uma matriz
 * de distâncias pré-carregadas em memória.
 */
void cvrp::CachedDistancesEvalFunc::operator()(metaheuristics::Encoding &chrom) {
  const size_t numDeliveries = this->cvrpInstance.deliveries.size();
  std::vector<std::vector<int32_t>> routes = this->decodeRoutes(chrom);
  double totalDistance = 0.0;
  for (auto route : routes) {
    double routeDistance = this->matrix->get(numDeliveries, route.at(0));
    for (size_t i = 0; i < route.size() - 1; i++) {
      uint32_t first = route.at(i);
      uint32_t second = route.at(i + 1);
      routeDistance += this->matrix->get(first, second);
    }
    routeDistance += this->matrix->get(numDeliveries, route.at(route.size() - 1));
    totalDistance += routeDistance;
  }
  chrom.fitness(1 / totalDistance);
}

std::vector<std::vector<int32_t>> cvrp::CachedDistancesEvalFunc::decodeRoutes(metaheuristics::Encoding &chrom) {
  std::vector<std::vector<int32_t>> routes(1, std::vector<int32_t>());
  double occupation = 0.0;
  size_t currentRouteIndex = 0;

  for (int32_t i : chrom) {
    auto currentDeliverySize = this->cvrpInstance.deliveries.at(i).size;
    if (occupation + currentDeliverySize > cvrpInstance.vehicle_capacity) {
      occupation = 0.0;
      routes.push_back(std::vector<int32_t>(1, i));
      currentRouteIndex++;
    } else {
      occupation += currentDeliverySize;
      routes.at(currentRouteIndex).push_back(i);
    }
  }
  return routes;
}
