#ifndef CVRP_ADJACENCY_MATRIX_HPP_
#define CVRP_ADJACENCY_MATRIX_HPP_

#include <loggibud/cvrp_instance.h>

#include <vector>

#include "routing_service.hpp"

namespace cvrp::graphs {

class Matrix : private std::vector<std::vector<double>> {
 public:
  Matrix(size_t len);

  void set(size_t i, size_t k, double value);
  double get(size_t i, size_t k) const;

 private:
  const size_t length;
};

Matrix* create_matrix(const loggibud::CVRPInstance& instance);

}  // namespace cvrp::graphs

#endif  // CVRP_ADJACENCY_MATRIX_HPP_
