#ifndef CVRP_DECODING_SERVICE_HPP_
#define CVRP_DECODING_SERVICE_HPP_

#include <loggibud/cvrp_instance.h>
#include <loggibud/cvrp_solution.h>

#include <metaheuristics/encoding.hpp>

using namespace loggibud;

namespace cvrp {

/**
 * @brief Classe que decodifica instâncias de soluções codificadas como
 * permutações em objetos de solução do CVRP segudo o esquema definido pelo
 * LoggiBUD.
 */
class DecodingService {
 public:
  /**
   * @brief Construct a new Cvrp Decoder object
   *
   * @param cvrpInstance uma referência para um objeto da classe CVRPInstance.
   */
  explicit DecodingService(CVRPInstance &cvrpInstance) : problem(cvrpInstance) {}

  CVRPSolution decode(const metaheuristics::Encoding &chrom);

 private:
  CVRPInstance &problem;
};

}  // namespace cvrp

#endif  // CVRP_DECODING_SERVICE_HPP_
