#include "decoding_service.hpp"

CVRPSolutionVehicle* newEmptyVehicle(const Point origin);
size_t countTotalDeliveriesInSolution(CVRPSolution* s);

/**
 * @brief Decodifica uma permutação em um objeto que representa a solução
 * do CVRP baseado no schema definido pelo LoggiBUD.
 *
 * @param chrom instância de cromossomo (permutação)
 * @return instância de solução do problema criado pelo LoggiBUD.
 */
CVRPSolution cvrp::DecodingService::decode(const metaheuristics::Encoding& chrom) {
  CVRPSolution solution{};
  const size_t numDeliveries = this->problem.deliveries.size();

  if (chrom.size() != numDeliveries) {
    throw std::logic_error("Chromosome not have a valid size");
  }

  const size_t maxCapacity = this->problem.vehicle_capacity;
  CVRPSolutionVehicle* vehicle = newEmptyVehicle(this->problem.origin);

  for (size_t i : chrom) {
    Delivery currentDelivery = this->problem.deliveries[i];
    if (vehicle->occupation() + currentDelivery.size > maxCapacity) {
      CVRPSolutionVehicle vehicle_ = *vehicle;
      solution.vehicles.push_back(vehicle_);
      vehicle = newEmptyVehicle(this->problem.origin);
    }
    vehicle->deliveries.push_back(currentDelivery);
  }

  if (vehicle->deliveries.size() > 0) {
    solution.vehicles.push_back(*vehicle);
  }

  solution.name = this->problem.name;
  return solution;
}

size_t countTotalDeliveriesInSolution(CVRPSolution* s) {
  size_t count = 0;
  for (auto& v : s->vehicles) {
    count += v.deliveries.size();
  }
  return count;
}

CVRPSolutionVehicle* newEmptyVehicle(const Point origin) {
  CVRPSolutionVehicle* vehicle = new CVRPSolutionVehicle();
  vehicle->origin = origin;
  vehicle->deliveries = std::vector<Delivery>();
  return vehicle;
}