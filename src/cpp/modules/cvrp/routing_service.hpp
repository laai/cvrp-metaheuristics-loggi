#ifndef CVRP_ROUTING_SERVICE_HPP_
#define CVRP_ROUTING_SERVICE_HPP_

#include <loggibud/cvrp_solution_vehicle.h>

#include <memory>
#include <osrm/engine_config.hpp>
#include <osrm/json_container.hpp>
#include <osrm/osrm.hpp>
#include <osrm/route_parameters.hpp>
#include <osrm/status.hpp>

namespace cvrp {

class RoutingService {
 public:
  /**
   * Constructor da classe RoutingService
   * @param engine uma referência para um RoutingEngine.
   */
  RoutingService(std::shared_ptr<osrm::OSRM> osrmEngine) : osrmEngine(osrmEngine) {}

  double getVehicleRouteDistance(loggibud::CVRPSolutionVehicle&);
  double distanceBetween(const loggibud::Point&, const loggibud::Point&);
  double distanceBetween(const float, const float, const float, const float);

  static RoutingService* loadFromEnv() {
    const char* osrmDataPath = std::getenv("OSRM_DATA_PATH");
    if (osrmDataPath == NULL) {
      throw std::runtime_error("env var OSRM_DATA_PATH is not defined");
    }
    osrm::EngineConfig osrmConfig;
    osrmConfig.storage_config = {osrmDataPath};
    osrmConfig.use_shared_memory = false;
    osrmConfig.algorithm = osrm::EngineConfig::Algorithm::CH;
    auto osrmEngine = std::shared_ptr<osrm::OSRM>(new osrm::OSRM(osrmConfig));
    return new RoutingService(osrmEngine);
  }

 private:
  osrm::RouteParameters params_;
  std::shared_ptr<osrm::OSRM> osrmEngine;
};

}  // namespace cvrp

#endif  // CVRP_ROUTING_SERVICE_HPP_
