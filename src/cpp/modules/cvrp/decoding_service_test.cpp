#include "decoding_service.hpp"

#include <gtest/gtest.h>

#include <random>

using cvrp::DecodingService;

class DecodingServiceTest : public ::testing::Test {
 protected:
  loggibud::CVRPInstance cvrpInstanceMock;
  size_t numberOfDeliveries;
  DecodingService* service;

  void SetUp() override {
    cvrpInstanceMock.name = "mocked-name-of-instance";
    cvrpInstanceMock.origin.lat = 0.0;
    cvrpInstanceMock.origin.lng = 0.0;
    cvrpInstanceMock.vehicle_capacity = 50;
    cvrpInstanceMock.deliveries.push_back(createDeliveryMock(18));
    cvrpInstanceMock.deliveries.push_back(createDeliveryMock(19));
    cvrpInstanceMock.deliveries.push_back(createDeliveryMock(17));
    cvrpInstanceMock.deliveries.push_back(createDeliveryMock(12));
    cvrpInstanceMock.deliveries.push_back(createDeliveryMock(18));
    cvrpInstanceMock.deliveries.push_back(createDeliveryMock(15));
    cvrpInstanceMock.region = "sp";
    numberOfDeliveries = cvrpInstanceMock.deliveries.size();
    service = new DecodingService(cvrpInstanceMock);
  }

  loggibud::Delivery createDeliveryMock(size_t size) {
    loggibud::Delivery delivery{};
    delivery.id = "awesome-id";
    delivery.point.lat = -1.42;
    delivery.point.lng = -40.1246;
    delivery.size = size;
    return delivery;
  }

  metaheuristics::Encoding createEncodingMock(size_t size) {
    metaheuristics::Encoding encoding(size);
    for (uint32_t i = 0; i < size; i++) {
      encoding[i] = i;
    }
    std::random_shuffle(encoding.begin(), encoding.end());
    return encoding;
  }
};

TEST_F(DecodingServiceTest, shouldReturnASolutionWithSameName) {
  metaheuristics::Encoding encoding = createEncodingMock(numberOfDeliveries);
  loggibud::CVRPSolution solution = service->decode(encoding);
  EXPECT_EQ(solution.name, cvrpInstanceMock.name);
}

TEST_F(DecodingServiceTest, shouldAllVehiclesHavingOccupationLessOrEqualCapacity) {
  metaheuristics::Encoding encoding = createEncodingMock(numberOfDeliveries);
  loggibud::CVRPSolution solution = service->decode(encoding);
  for (loggibud::CVRPSolutionVehicle& vehicle : solution.vehicles) {
    EXPECT_LE(vehicle.occupation(), cvrpInstanceMock.vehicle_capacity);
  }
}

TEST_F(DecodingServiceTest, shouldSolutionHaveNumDeliveriesEqualToChromSize) {
  metaheuristics::Encoding encoding = createEncodingMock(numberOfDeliveries);
  loggibud::CVRPSolution solution = service->decode(encoding);

  size_t sum = 0;
  for (loggibud::CVRPSolutionVehicle& vehicle : solution.vehicles) {
    sum += vehicle.deliveries.size();
  }
  EXPECT_EQ(sum, numberOfDeliveries);
}
