#include "routing_service.hpp"

using osrm::json::Array;
using osrm::json::Number;
using osrm::json::Object;
using osrm::util::Coordinate;
using osrm::util::FloatLatitude;
using osrm::util::FloatLongitude;

namespace engine_api = osrm::engine::api;

/**
 * @brief Calcula a distância percorrida pela rota de um veículo. Recebe uma instância da classe
 * Vehicle que possui um ponto de origem e uma lista de pontos (entregas).
 *
 * @param vehicle instância do veículo
 * @return double distância percorrida em Km.
 */
double cvrp::RoutingService::getVehicleRouteDistance(loggibud::CVRPSolutionVehicle& vehicle) {
  double distance = this->distanceBetween(vehicle.origin, vehicle.deliveries[0].point);

  auto currentDelivery = vehicle.deliveries.begin();
  auto nextDelivery = vehicle.deliveries.begin() + 1;
  auto lastDelivery = vehicle.deliveries.end() - 1;
  for (; currentDelivery < lastDelivery; ++currentDelivery) {
    distance += this->distanceBetween(currentDelivery->point, nextDelivery->point);
    ++nextDelivery;
  }
  return distance;
}

/**
 * Calcula a distância entre dois pares de coordenadas (longitude, latitude).
 * @return o valor da distância como double.
 */
double cvrp::RoutingService::distanceBetween(const float originX, const float originY,
                                             const float targetX, const float targetY) {
  Coordinate origin, target;
  engine_api::ResultT result = Object();

  origin = Coordinate{FloatLongitude{originX}, FloatLatitude{originY}};
  target = Coordinate{FloatLongitude{targetX}, FloatLatitude{targetY}};
  this->params_.coordinates.push_back(origin);
  this->params_.coordinates.push_back(target);

  const osrm::Status status = this->osrmEngine->Route(this->params_, result);
  this->params_.coordinates.clear();

  // Retorna infiniito caso ocorra erro e loga na saída pdrão de erros
  if (status == osrm::Status::Error) {
    std::cerr << "Error on routing process!" << std::endl;
    return std::numeric_limits<double>::infinity();
  }

  auto routes = result.get<Object>().values["routes"];
  auto first_route = routes.get<Array>().values[0].get<Object>().values;

  return first_route["distance"].get<Number>().value;
}

/**
 * Calcula a distância entre dois pares de coordenadas representados por objetos Point (longitude,
 * latitude).
 * @return o valor da distância como double.
 */
double cvrp::RoutingService::distanceBetween(const loggibud::Point& firstPoint,
                                             const loggibud::Point& secondPoint) {
  return this->distanceBetween(firstPoint.lng, firstPoint.lat, secondPoint.lng, secondPoint.lat);
}