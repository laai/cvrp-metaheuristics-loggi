#include "json.hpp"

#include <gtest/gtest.h>

using namespace helpers;

TEST(JsonHelperTest, shouldConvertCVRPInstanceToJSONString) {
  loggibud::CVRPInstance instance{};
  instance.name = "name_of_instance";
  instance.region = "some_region";
  instance.origin.lat = 0.0;
  instance.origin.lng = 0.0;
  instance.vehicle_capacity = 200;
  instance.deliveries = std::vector<loggibud::Delivery>();

  loggibud::Delivery delivery;
  delivery.id = "id";
  delivery.point.lat = 1.0;
  delivery.point.lng = -2.0;
  delivery.size = 10;
  instance.deliveries.push_back(delivery);

  std::string expectedJson(
      "{\"name\":\"name_of_instance\",\"region\":\"some_region\",\"origin\":{\"lat\":0.0,\"lng\":0."
      "0},\"vehicle_capacity\":200,\"deliveries\":[{\"id\":\"id\",\"point\":{\"lat\":1.0,\"lng\":-"
      "2.0},\"size\":10}]}");

  std::string jsonStr = json::jsonifyCvrpInstance(instance);

  EXPECT_EQ(expectedJson, jsonStr);
}

TEST(JsonHelperTest, shouldConvertCVRPSolutionToJSONString) {
  loggibud::Delivery delivery;
  delivery.id = "id";
  delivery.point.lat = 1.0;
  delivery.point.lng = -2.0;
  delivery.size = 10;

  loggibud::CVRPSolutionVehicle vehicle{};
  vehicle.origin.lat = 0.0;
  vehicle.origin.lng = 0.0;
  vehicle.deliveries.push_back(delivery);

  loggibud::CVRPSolution solution{};
  solution.name = "name_of_instance";
  solution.vehicles.push_back(vehicle);

  std::string expectedJson(
      "{\"name\":\"name_of_instance\",\"vehicles\":[{\"origin\":{\"lat\":0.0,\"lng\":0.0},"
      "\"deliveries\":[{\"id\":\"id\",\"point\":{\"lat\":1.0,\"lng\":-"
      "2.0},\"size\":10}]}]}");

  std::string jsonStr = json::jsonifyCvrpSolution(solution);

  EXPECT_EQ(expectedJson, jsonStr);
}

TEST(JsonHelperTest, shouldConvertConvergenceToStringJsonArray) {
  std::vector<double> convergence({1.2, 3.21, 9.999});
  std::string expectedJson("[1.200,3.210,9.999]");
  std::string jsonStr = json::jsonifyConvergence(convergence);
  EXPECT_EQ(expectedJson, jsonStr);
} 