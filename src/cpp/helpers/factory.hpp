#ifndef HELPERS_FACTORY_HPP_
#define HELPERS_FACTORY_HPP_

#include <chrono>
#include <loggibud/cvrp_instance.h>
#include <loggibud/cvrp_solution.h>

#include "json.hpp"
#include "../modules/ioc/container.hpp"
#include "../modules/metaheuristics/encoding.hpp"
#include "../modules/metaheuristics/convergence_storage_eval_continue.hpp"

using metaheuristics::ConvergenceStorageEvalContinue;

namespace helpers::factory {

repository::models::ExperimentResult createExperimentResult(ioc::Container *c, metaheuristics::Encoding& bestElement, std::string algorithmName, std::chrono::milliseconds ms) {
  repository::models::ExperimentResult resultModel;
  std::vector<double> convergence = c->getBean<ConvergenceStorageEvalContinue>()->getConvergence();
  loggibud::CVRPInstance cvrpInstance = *(c->getBean<loggibud::CVRPInstance>());
  loggibud::CVRPSolution cvrpSolution = c->getSolution(bestElement);
  double totalKilometers = 1 / bestElement.fitness();

  resultModel.algorithmName = algorithmName;
  resultModel.finalTotalDistance = totalKilometers;
  resultModel.elapsedTimeMs = ms.count();
  resultModel.instance = helpers::json::jsonifyCvrpInstance(cvrpInstance);
  resultModel.solution = helpers::json::jsonifyCvrpSolution(cvrpSolution);
  resultModel.convergence = helpers::json::jsonifyConvergence(convergence);
  
  return resultModel;
}

}

#endif  // HELPERS_FACTORY_HPP_
