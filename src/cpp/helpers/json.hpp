#ifndef HELPERS_JSON_HPP_
#define HELPERS_JSON_HPP_

#include <loggibud/cvrp_instance.h>
#include <loggibud/cvrp_solution.h>
#include <rapidjson/document.h>
#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

#include <iomanip>
#include <numeric>
#include <sstream>
#include <string>

namespace helpers::json {

/**
 * @brief Convert a CVRPInstance object to it JSON string.
 *
 * @param instance the instance of CVRPInstance class.
 */
std::string jsonifyCvrpInstance(loggibud::CVRPInstance& instance) {
  using namespace rapidjson;
  StringBuffer buffer;
  Writer<StringBuffer> writer(buffer);
  Document doc;

  doc.SetObject();
  auto& allocator = doc.GetAllocator();

  doc.SetObject();
  {
    Value v;
    v.SetString(instance.name.c_str(), allocator);
    doc.AddMember("name", v, allocator);
  }
  {
    Value v;
    v.SetString(instance.region.c_str(), allocator);
    doc.AddMember("region", v, allocator);
  }
  {
    Value v;
    v.SetObject();
    v.AddMember("lat", Value(instance.origin.lat), allocator);
    v.AddMember("lng", Value(instance.origin.lng), allocator);
    doc.AddMember("origin", v, allocator);
  }
  doc.AddMember("vehicle_capacity", Value(instance.vehicle_capacity), allocator);
  {
    Value v;
    v.SetArray();
    for (loggibud::Delivery& d : instance.deliveries) {
      Value id;
      id.SetString(d.id.c_str(), allocator);

      Value point;
      point.SetObject();
      point.AddMember("lat", Value(d.point.lat), allocator);
      point.AddMember("lng", Value(d.point.lng), allocator);

      Value elementValue;
      elementValue.SetObject();
      elementValue.AddMember("id", id, allocator);
      elementValue.AddMember("point", point, allocator);
      elementValue.AddMember("size", Value(d.size), allocator);

      v.PushBack(elementValue, allocator);
    }
    doc.AddMember("deliveries", v, allocator);
  }

  doc.Accept(writer);
  return std::string(buffer.GetString(), buffer.GetSize());
}

std::string jsonifyCvrpSolution(loggibud::CVRPSolution& solution) {
  using namespace rapidjson;
  StringBuffer buffer;
  Writer<StringBuffer> writer(buffer);
  Document doc;
  auto& allocator = doc.GetAllocator();

  doc.SetObject();
  {
    Value v;
    v.SetString(solution.name.c_str(), allocator);
    doc.AddMember("name", v, allocator);
  }

  Value vehicles;
  vehicles.SetArray();
  for (loggibud::CVRPSolutionVehicle& v : solution.vehicles) {
    Value origin;
    origin.SetObject();
    origin.AddMember("lat", Value(v.origin.lat), allocator);
    origin.AddMember("lng", Value(v.origin.lng), allocator);

    Value deliveries;
    deliveries.SetArray();
    for (loggibud::Delivery& d : v.deliveries) {
      Value id;
      id.SetString(d.id.c_str(), allocator);
      Value point;
      point.SetObject();
      point.AddMember("lat", Value(d.point.lat), allocator);
      point.AddMember("lng", Value(d.point.lng), allocator);
      Value delivery;
      delivery.SetObject();
      delivery.AddMember("id", id, allocator);
      delivery.AddMember("point", point, allocator);
      delivery.AddMember("size", Value(d.size), allocator);
      deliveries.PushBack(delivery, allocator);
    }

    Value element;
    element.SetObject();
    element.AddMember("origin", origin, allocator);
    element.AddMember("deliveries", deliveries, allocator);
    vehicles.PushBack(element, allocator);
  }
  doc.AddMember("vehicles", vehicles, allocator);

  doc.Accept(writer);
  return std::string(buffer.GetString(), buffer.GetSize());
}

std::string jsonifyConvergence(std::vector<double> values) {
  std::stringstream stream;
  stream << std::fixed << std::setprecision(3) << "[";
  std::vector<double>::const_iterator i = values.cbegin();
  for (; i < values.cend(); ++i) {
    if (*i == std::numeric_limits<double>::infinity()) {
      continue;
    }
    stream << *i;
    if (i < values.cend() - 1) {
      stream << ",";
    }
  }
  stream << "]";
  return stream.str();
}

}  // namespace helpers::json

#endif  // HELPERS_JSON_HPP_
