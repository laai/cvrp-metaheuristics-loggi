#ifndef HELPERS_TYPES_HPP_
#define HELPERS_TYPES_HPP_

#include <vector>

namespace helpers::types {

template <typename T>
struct triangle_matrix {
 public:
  triangle_matrix(size_t len) : size(len), _values(std::vector<T>((len + 1) * len / 2)) {}

  std::vector<T>& values() { return this->_values; }

  T& operator()(size_t i, size_t j) { return this->_values.at(i * (i + 1) / 2 + j); }

 private:
  const size_t size;
  std::vector<T> _values;
};

}  // namespace helpers::types

#endif  // HELPERS_TYPES_HPP_
