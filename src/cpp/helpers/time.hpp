#ifndef HELPERS_TIME_HPP_
#define HELPERS_TIME_HPP_

#include <chrono>
#include <functional>

namespace helpers::time {

/**
 * @brief Execute a function and returns the duration elapsed time of the running.
 *
 * @param lambda a function to run, should no have return or argument value
 * @return the duration of the function execution
 */
template <class DurationType>
inline std::chrono::milliseconds getElapsedTime(std::function<void()> lambda) {
  using std::chrono::system_clock;
  using std::chrono::time_point;

  time_point startMoment = system_clock::now();
  lambda();
  time_point finishMoment = system_clock::now();
  return std::chrono::duration_cast<DurationType>(finishMoment - startMoment);
}
}  // namespace helpers::time

#endif  // HELPERS_TIME_HPP_
