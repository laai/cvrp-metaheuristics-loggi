#ifndef APP_H_
#define APP_H_

#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/spdlog.h>
#include <sqlite3pp.h>

#include <controllers/config.hpp>
#include <controllers/execution_data.hpp>
#include <controllers/invoker.hpp>
#include <controllers/solver_factory.hpp>
#include <cstring>
#include <cvrp/adjacency_matrix.hpp>
#include <string>

class App {
 private:
  loggibud::CVRPInstance instance;
  std::string algorithmKey;
  std::string crossoverKey;
  double sortRate;
  std::string database;
  unsigned int repeatTimes;
  controllers::SolverFactory* solverFactory;
  cvrp::graphs::Matrix* distancesMatrix;

 public:
  App(std::string instanceName, std::string algorithmKey, std::string crossoverKey, double sortRate, std::string databaseName,
      unsigned int repeatTimes)
      : instance(loggibud::CVRPInstance::from_file(instanceName.c_str())),
        algorithmKey(algorithmKey),
        crossoverKey(crossoverKey),
        sortRate(sortRate),
        database(databaseName),
        repeatTimes(repeatTimes) {}

  ~App() {}

  int run() {
    const char* propertiesFile = std::getenv("PROPERTIES_FILE");
    if (std::strlen(propertiesFile) == 0) {
      throw std::runtime_error("PROPERTIES_FILE variable is undefined");
    }
    auto cfg = controllers::loadConfiguration(propertiesFile);

    solverFactory = controllers::SolverFactory::getFactory(algorithmKey.c_str(), &cfg, crossoverKey);

    if (!controllers::is_valid_crossover(this->crossoverKey)) {
      throw std::runtime_error(fmt::format("Unknown crossover operator: {}", this->crossoverKey));
    }

    distancesMatrix = cvrp::graphs::create_matrix(instance);

    for (unsigned int i = 0; i < repeatTimes; i++) {
      controllers::ExecutionData::reset();
      controllers::Solver* solver = solverFactory->createSolver(&this->instance, this->distancesMatrix);
      controllers::Invoker(solver).invoke(&this->instance);

      if (!this->database.empty()) {
        spdlog::info("Connecting to Database in {}", this->database);
        sqlite3pp::database* db = new sqlite3pp::database(this->database.c_str());
        controllers::ExecutionData::save(db);
      }
      delete solver;
    }

    return EXIT_SUCCESS;
  }
};

#endif  // APP_H_
