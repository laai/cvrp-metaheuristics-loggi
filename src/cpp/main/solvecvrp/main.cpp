#define BLUE_LOG_FORMAT "\033[36m%D %T [%l] %n\033[0m %v"
#define RED_LOG_FORMAT "\033[31m%D %T [%l] %n\033[0m %v"

#include <args.hxx>
using namespace args;
using Opts = args::Options;
using std::string;

#define INSTANCE_HELP "The path to JSON file instance"
#define ALGORITHM_HELP "The optimization algorithm to use (ga|grasp|hybrid)"
#define CROSSOVER_HELP "Crossover operator (only for algorithm=ga)"
#define SORTING_RATE_HELP "Greedy Order Crossover Sorting Rate in percent (%) [0.0 > s >= 1.0]"
#define DATABASE_HELP "The path to the database file to store"
#define REPEAT_HELP "number of times to repeat the execution of the selected algorithm"

#include "app.h"

void assertOptions(ValueFlag<string>& algorithm, ValueFlag<string>& crossover, ValueFlag<double>& sortRate) {
  if (crossover && algorithm.Get().compare("ga") != 0) {
    throw ParseError("options --crossover or -x is allowed only for algorithm=ga");
  }
  if (sortRate && crossover.Get().compare("GOX") != 0) {
    throw ParseError("option --crossover-sorting-rate is allowed only for crossover=GOX");
  }
}

int main(int argc, const char* argv[]) {
  spdlog::set_pattern(BLUE_LOG_FORMAT);
  ArgumentParser parser("Optimize the CVRP solving");
  HelpFlag help(parser, "help", "Display this help menu", {'h', "help"});
  Positional<string> instance(parser, "instance", INSTANCE_HELP);
  ValueFlag<string> algorithm(parser, "algorithm", ALGORITHM_HELP, {'a', "algorithm"}, Opts::Required);
  ValueFlag<string> crossover(parser, "crossover", CROSSOVER_HELP, {'x', "crossover"});
  ValueFlag<double> sortRate(parser, "sorting-rate", SORTING_RATE_HELP, {"crossover-sorting-rate"});
  ValueFlag<string> database(parser, "database", DATABASE_HELP, {'d', "database"});
  ValueFlag<unsigned int> repeat(parser, "repeat", REPEAT_HELP, {"repeat"});

  try {
    parser.ParseCLI(argc, argv);
    assertOptions(algorithm, crossover, sortRate);
    string cvrpInstanceFile = args::get(instance);
    string algorithm_ = args::get(algorithm);
    string crossover_ = args::get(crossover);
    double sortRate_ = sortRate ? args::get(sortRate) : 0.1;
    string databaseFile = args::get(database);
    unsigned int repeatTimes = repeat ? args::get(repeat) : 1L;
    return App(cvrpInstanceFile, algorithm_, crossover_, sortRate_, databaseFile, repeatTimes).run();
  } catch (const Help&) {
    std::cout << parser;
    return 0;
  } catch (const ParseError& e) {
    std::cerr << e.what() << std::endl;
    std::cerr << parser;
    return 1;
  } catch (std::exception& e) {
    spdlog::set_pattern(RED_LOG_FORMAT);
    spdlog::error(e.what());
    return EXIT_FAILURE;
  }
}
