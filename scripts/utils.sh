TEMP_DIR=.temp

gray="\e[38;5;245m"
red="\e[38;5;9m"
green="\e[38;5;82m"
reset="\e[0m"

logrun() {
  comando=$1
  echo -e "$ $comando $gray"
  $comando
  if [ $? -eq 0 ]
  then
    echo -en "$green"
    echo -e "$ $comando $reset\n"
  else
    echo -en "$red"
    echo -e "$ $comando $reset\n"
    exit $?
  fi
}

check_dep_tools() {
    DEPS=(curl wget unzip cmake)

    for d in ${DEPS[@]}; do
    if [[ -f /usr/bin/$d ]]; then
        echo -e "${gray}[ ${green}OK ${gray}] $d foi encontrado${reset}"
    else
        echo -e "Garanta que todos esses progrmas estejam instalados:\n- ${DEPS[@]}"
        exit 1
    fi
    done
}

create_temp_dir() {
    logrun "mkdir -p $TEMP_DIR"
}

delete_temp_dir() {
    logrun "rm -rfv $TEMP_DIR"
}

github_get() {
    repository=$1
    tag=$2
    zipfilename=$3

    logrun "curl -ksL https://github.com/${repository}/archive/refs/tags/${tag}.zip --output ${zipfilename}"
}

if_fails() {
    if [ $? -ne 0 ]; then
        echo $1
        exit 1
    fi
}