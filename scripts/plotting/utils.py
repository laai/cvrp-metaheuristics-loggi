import pandas
import sqlite3
import argparse
import numpy

DATABASE_MACHINES = {
    "MACOS": "data/results-macos.db",
    "LINUX1": "data/results-linux-ufpa.db",
    "LINUX2": "data/results-linux-filipe.db",
    "WIN10": "data/results-windows.db",
}


def read_table(cnx: sqlite3.Connection):
    return pandas.read_sql_query("SELECT * FROM experiment_results", con=cnx)


def get_higher_size(matrix):
    return numpy.max([len(lst) for lst in matrix])


def get_default_argparser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser()
    parser.add_argument("database", help="path to the sqlite database file", type=str)
    parser.add_argument("-a", "--algorithm", help="algorithm id", type=str)
    parser.add_argument(
        "-i", "--instance", help="name of the loggi instance", type=str, required=True
    )
    return parser
