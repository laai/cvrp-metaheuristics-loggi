import pandas
from matplotlib import pyplot
from scripts.plotting.strip_charts import get_data_from_database
from scripts.plotting.utils import DATABASE_MACHINES as machines


def get_results_by_machine(df: pandas.DataFrame, machine: str) -> pandas.DataFrame:
    return df.loc[df["Machine"] == machine].copy(deep=True)


def filter_by_algorithm(d: pandas.DataFrame, algo: str):
    return d.loc[d["Algorithm"] == algo].copy(deep=True)


def get_result_list(d: pandas.DataFrame, algo: str, mach: str) -> pandas.Series:
    return filter_by_algorithm(get_results_by_machine(df, mach), algo)


if __name__ == "__main__":
    pyplot.style.use("seaborn-v0_8")
    df = pandas.concat([get_data_from_database(machines[m], m) for m in machines])

    results_key = "Final Distance of Solution"

    boxplot_conf = dict(
        widths=0.33,
        patch_artist=True,
        showmeans=True,
        medianprops=dict(color="darkblue"),
        boxprops=dict(facecolor="#fff", color="darkblue"),
    )

    fig, axes = pyplot.subplots(1, 2, figsize=(12, 7), sharey=True)
    axes[0].set_xticklabels(machines)
    axes[1].set_xticklabels(machines)

    ga_mac_data = get_result_list(df, "genetic_algorithm", "MACOS")
    grasp_mac_data = get_result_list(df, "grasp", "MACOS")

    axes[0].boxplot(
        [
            get_result_list(df, "genetic_algorithm", machine)[results_key]
            for machine in machines
        ],
        **boxplot_conf,
    )
    axes[1].boxplot(
        [get_result_list(df, "grasp", machine)[results_key] for machine in machines],
        **boxplot_conf,
    )
    pyplot.show()

    if input("Save figure? [y/N] ") == "y":
        name = input("Name of file: ")
        fig.savefig(name)
