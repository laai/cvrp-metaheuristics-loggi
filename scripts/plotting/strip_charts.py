import pandas
import sqlite3
from string import Template
import plotly.express as px

QUERY_TEMPLATE = Template(
    """SELECT algorithm_name as Algorithm, instance_name as Instance, final_total_distance as 'Final Distance of Solution'
  FROM (
	SELECT * FROM experiment_results er
	WHERE algorithm_name = "$algorithm"
	AND instance_name = "$instance"
	LIMIT 10
  );
"""
)


def get_data(algorithm: str, instance: str, database: str) -> pandas.DataFrame:
    q = QUERY_TEMPLATE.substitute({"algorithm": algorithm, "instance": instance})
    return pandas.read_sql_query(q, sqlite3.connect(database))


def get_data_from_database(database: str, id: str) -> pandas.DataFrame:
    dataframes = [
        get_data("genetic_algorithm", "cvrp-0-pa-116", database),
        get_data("grasp", "cvrp-0-pa-116", database),
        get_data("genetic_algorithm", "cvrp-0-pa-111", database),
        get_data("grasp", "cvrp-0-pa-111", database),
    ]
    df = pandas.concat(dataframes, ignore_index=True)
    df["Machine"] = len(df) * [id]
    return df


if __name__ == "__main__":
    df = pandas.concat(
        [
            get_data_from_database("data/results-reproducibility/MACOS.db", "MACOS"),
            get_data_from_database("data/results-reproducibility/LINUX1.db", "LINUX1"),
            get_data_from_database("data/results-reproducibility/LINUX2.db", "LINUX2"),
            get_data_from_database("data/results-reproducibility/WIN10.db", "WIN10"),
        ]
    )

    print(df)
    fig = px.strip(
        df,
        x="Instance",
        y="Final Distance of Solution",
        color="Algorithm",
        facet_col="Machine",
        width=1200,
        height=600,
    )
    fig.show()
