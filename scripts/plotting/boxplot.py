import pandas
import sqlite3
import argparse
import json
import numpy
from string import Template
from matplotlib import pyplot
from matplotlib.pyplot import Axes

QUERY_TEMPLATE = Template(
    """SELECT algorithm_name, instance_name, COUNT(id), JSON_GROUP_ARRAY(final_total_distance) as distances
  FROM (
	SELECT * FROM experiment_results er
	WHERE algorithm_name = "$algorithm"
	AND instance_name = "$instance"
	LIMIT 10
  )
  GROUP BY 1, 2;
"""
)


parser = argparse.ArgumentParser()
parser.add_argument("database", help="name of the sqlite file")


def get_data(algorithm: str, instance: str) -> pandas.DataFrame:
    q = QUERY_TEMPLATE.substitute({"algorithm": algorithm, "instance": instance})
    return pandas.read_sql_query(q, sqlite3.connect(args.database))


def get_algorithm_results_by_instance(instance: str) -> pandas.DataFrame:
    algorithms = ["genetic_algorithm", "grasp"]
    dataframes = [get_data(alg, instance) for alg in algorithms]
    d = {
        algorithms[i].upper(): json.loads(dataframes[i]["distances"][0])
        for i in range(2)
    }
    return pandas.DataFrame(data=d)


def create_boxplot(d: pandas.DataFrame, ax: Axes, title: str):
    boxplot_conf = dict(
        return_type="dict",
        widths=0.33,
        patch_artist=True,
        showmeans=True,
        boxprops=dict(linestyle="-", color="darkblue", facecolor="white"),
        medianprops=dict(color="darkblue"),
    )

    plot = d.boxplot(ax=ax, vert=False, **boxplot_conf)
    ax.grid(axis="y")
    ax.set_title(title)
    return plot


if __name__ == "__main__":
    pyplot.style.use("seaborn-v0_8")
    args = parser.parse_args()

    fig, axes = pyplot.subplots(2, 1, figsize=(14, 6), sharex=True)

    results_cvrp_116 = get_algorithm_results_by_instance("cvrp-0-pa-116")
    results_cvrp_111 = get_algorithm_results_by_instance("cvrp-0-pa-111")

    plot1 = create_boxplot(results_cvrp_116, axes[0], "Instance: cvrp-0-pa-116")
    plot2 = create_boxplot(results_cvrp_111, axes[1], "Instance: cvrp-0-pa-111")

    axes[1].set_xlabel("Distance sum of all routes")
    axes[1].set_xlim(1.3e6, 2.3e6)
    axes[1].set_xticks(numpy.linspace(1.3e6, 2.3e6, 21))

    pyplot.show()
