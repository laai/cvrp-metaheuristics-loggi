from string import Template

BASE_QUERY_TEMPLATE = Template(
    """SELECT
	instance_name as "Instance",
	algorithm_name as "Algorithm",
	-- count(id) as "Count",
	((ROUND(AVG(elapsed_time_ms)) / 100) / 60) / 60 as "Average in Hours",
	(ROUND(STDEV(elapsed_time_ms)) / 100) / 60 as "St. Deviation in Minutes"
FROM (SELECT * FROM experiment_results er WHERE algorithm_name = "$algorithm" AND instance_name = "$instance" LIMIT 10)
GROUP BY 1, 2
ORDER BY 2 DESC;
"""
)

tuples = [
    ("cvrp-0-pa-116", "genetic_algorithm"),
    ("cvrp-0-pa-116", "grasp"),
    ("cvrp-0-pa-111", "genetic_algorithm"),
    ("cvrp-0-pa-111", "grasp"),
]


def make_query(algorithm, instance) -> str:
    return BASE_QUERY_TEMPLATE.substitute(
        {"algorithm": algorithm, "instance": instance}
    )
