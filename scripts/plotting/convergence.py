import numpy
import pandas
import sqlite3
import json
from typing import Tuple, List
from pandas import Series, read_sql_query
from matplotlib import pyplot
from scripts.plotting.utils import get_higher_size, get_default_argparser


def get_stats(index: int, matrix: List[List[float]]) -> Tuple[float, float, float]:
    indexed_values = [lst[index] for lst in matrix]
    return (
        numpy.min(indexed_values),
        numpy.average(indexed_values),
        numpy.max(indexed_values),
    )


def get_convergence_list(convergences: Series):
    convergence_list = [json.loads(conv) for conv in convergences]
    higher_size = get_higher_size(convergence_list)
    for conv in convergence_list:
        while len(conv) < higher_size:
            conv.append(conv[-1])
    return convergence_list


def read_convergence_from_database(
    database: str, algorithm: str, instance: str
) -> pandas.Series:
    q = f"SELECT convergence FROM experiment_results WHERE algorithm_name = '{algorithm}' AND instance_name = '{instance}'"
    table = read_sql_query(q, con=sqlite3.connect(database))
    return table["convergence"]


def plot_convergence(
    axes: pyplot.Axes,
    database: str,
    algorithm: str,
    instance: str,
    color: str = "black",
    marker: str = ".",
    with_title: bool = False,
    xlabel: str = "",
    ylabel: str = "",
):
    pyplot.style.use("seaborn-v0_8")
    convergence = read_convergence_from_database(database, algorithm, instance)

    convergence_list = get_convergence_list(convergence)
    min_values, max_values, avg_values = [], [], []

    for i in range(len(convergence_list[0])):
        min_, avg_, max_ = get_stats(i, convergence_list)
        min_values.append(min_)
        avg_values.append(avg_)
        max_values.append(max_)

    indices = numpy.arange(0, len(convergence_list[0]))
    axes.fill_between(indices, min_values, max_values, color=color, alpha=0.15)
    axes.scatter(indices, avg_values, marker=marker, color=color)
    axes.set_xticks(numpy.linspace(0, len(indices), 11))

    if xlabel != "":
        axes.set_xlabel(xlabel)

    if ylabel != "":
        axes.set_ylabel(ylabel)

    if with_title:
        axes.set_title(f"Convergence average of the {algorithm} for {instance}")


if __name__ == "__main__":
    parser = get_default_argparser()
    args = parser.parse_args()

    if args.algorithm is None:
        print("Missing argument --algorithm")
        exit(1)

    f, ax = pyplot.subplots(1, 1)
    plot_convergence(
        ax,
        args.database,
        algorithm=args.algorithm,
        instance=args.instance,
        with_title=True,
        xlabel="Iteration/Generation",
        ylabel="Distance of vehicle routes",
    )
    pyplot.show()
