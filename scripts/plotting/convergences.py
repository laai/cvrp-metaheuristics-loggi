from matplotlib import pyplot
from scripts.plotting.utils import get_default_argparser
from scripts.plotting.convergence import plot_convergence
from scripts.plotting.utils import DATABASE_MACHINES


def plot_ga_convergence(axes: pyplot.Axes, db: str, instance: str):
    plot_convergence(axes, db, "genetic_algorithm", instance)
    axes.set_ylabel("Distance of vehicle routes")


def plot_grasp_convergence(axes: pyplot.Axes, db: str, instance: str):
    plot_convergence(axes, db, "grasp", instance)


if __name__ == "__main__":
    pyplot.style.use("seaborn-v0_8")
    parser = get_default_argparser()

    fig, ax = pyplot.subplots(4, 2, figsize=(15, 12), sharey=True, sharex=False)

    ax[0][0].set_title("Convergences of Genetic Algorithm for cvrp-0-pa-116")
    ax[0][1].set_title("Convergences of GRASP for cvrp-0-pa-116")
    ax[3][0].set_xlabel("Generations")
    ax[3][1].set_xlabel("Iterations")

    plot_ga_convergence(ax[0][0], "data/results-reproducibility/MACOS.db", "cvrp-0-pa-116")
    plot_ga_convergence(ax[1][0], "data/results-reproducibility/LINUX1.db", "cvrp-0-pa-116")
    plot_ga_convergence(ax[2][0], "data/results-reproducibility/LINUX2.db", "cvrp-0-pa-116")
    plot_ga_convergence(ax[3][0], "data/results-reproducibility/WIN10.db", "cvrp-0-pa-116")

    plot_grasp_convergence(ax[0][1], "data/results-reproducibility/MACOS.db", "cvrp-0-pa-116")
    plot_grasp_convergence(ax[1][1], "data/results-reproducibility/LINUX1.db", "cvrp-0-pa-116")
    plot_grasp_convergence(ax[2][1], "data/results-reproducibility/LINUX2.db", "cvrp-0-pa-116")
    plot_grasp_convergence(ax[3][1], "data/results-reproducibility/WIN10.db", "cvrp-0-pa-116")

    ax[0][0].set_ylabel("MACOS")
    ax[1][0].set_ylabel("LINUX1")
    ax[2][0].set_ylabel("LINUX2")
    ax[3][0].set_ylabel("WIN10")

    pyplot.show()
    # fig.tight_layout()

    if input("Save figure? [y/N] ") == "y":
        name = input("Name of file: ")
        fig.savefig(name, bbox_inches="tight")
