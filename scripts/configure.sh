#!/bin/sh

source "$(pwd)/$(dirname "$0")/utils.sh"

START_DIR=$(pwd)

check_dep_tools
create_temp_dir

echo 'Installing..............................................................'
echo '  _                       _______ _   _______ '
echo ' | |                     (_) ___ \ | | |  _  \'
echo ' | |     ___   __ _  __ _ _| |_/ / | | | | | |'
echo ' | |    / _ \ / _` |/ _` | | ___ \ | | | | | |'
echo ' | |___| (_) | (_| | (_| | | |_/ / |_| | |/ / '
echo ' \_____/\___/ \__, |\__, |_\____/ \___/|___/  '
echo '               __/ | __/ |                    '
echo '              |___/ |___/                     '
echo '........................................................................'
LBUD_NAME='libloggibud'
LBUD_TAG='v1.0.2'
ZIPFILENAME=$TEMP_DIR/lbud.zip

github_get ropinho/libloggibud $LBUD_TAG "$ZIPFILENAME"

if [[ -f $ZIPFILENAME ]]; then
    logrun "unzip $ZIPFILENAME -d $TEMP_DIR"
    logrun "rm $ZIPFILENAME"
    logrun "cd $TEMP_DIR/$LBUD_NAME-1.0.2"
    logrun "pwd"
else
    echo 'Error on downloading the .zip file'
    exit 1
fi

logrun "cmake -S . -B build"
logrun "cmake --build build --config Devel --target install"
logrun "cd $START_DIR"
logrun "rm -r $TEMP_DIR/$LBUD_NAME-1.0.2"

# echo 'Configuring libOSRM.....................................................'
# echo ' ▒█████    ██████  ██▀███   ███▄ ▄███▓'
# echo '▒██▒  ██▒▒██    ▒ ▓██ ▒ ██▒▓██▒▀█▀ ██▒'
# echo '▒██░  ██▒░ ▓██▄   ▓██ ░▄█ ▒▓██    ▓██░'
# echo '▒██   ██░  ▒   ██▒▒██▀▀█▄  ▒██    ▒██ '
# echo '░ ████▓▒░▒██████▒▒░██▓ ▒██▒▒██▒   ░██▒'
# echo '░ ▒░▒░▒░ ▒ ▒▓▒ ▒ ░░ ▒▓ ░▒▓░░ ▒░   ░  ░'
# echo '  ░ ▒ ▒░ ░ ░▒  ░ ░  ░▒ ░ ▒░░  ░      ░'
# echo '░ ░ ░ ▒  ░  ░  ░    ░░   ░ ░      ░   '
# echo '    ░ ░        ░     ░            ░   '
# echo '........................................................................'

# TODO: Update Cmake to 3.18+

# OSRM_VERSION='v5.24.0'
# OSRM_GIT_URL='https://github.com/Project-OSRM/osrm-backend'

# mkdir -p .temp
# git clone --depth 1 --branch $OSRM_VERSION $OSRM_GIT_URL .temp/osrm-backend
# cd .temp/osrm-backend
# mkdir -p build && cd build


echo 'Installing..............................................................'
echo ' ______                   _ _      _____ _____ '
echo ' | ___ \                 | (_)    |  ___|  _  |'
echo ' | |_/ /_ _ _ __ __ _  __| |_ ___ | |__ | | | |'
echo ' |  __/ _` | `__/ _` |/ _` |/ ___||  __|| | | |'
echo ' | | | (_| | | | (_| | (_| | \__ \| |___\ \_/ /'
echo ' \_|  \__,_|_|  \__,_|\__,_|_|___/\____/ \___/ '
echo '........................................................................'

TAG='v3.0.0-beta'
ZIPFILENAME="$TEMP_DIR/paradiseo.zip"

github_get nojhan/paradiseo $TAG $ZIPFILENAME

if [[ -f $ZIPFILENAME ]]; then
    logrun "unzip $ZIPFILENAME -d $TEMP_DIR"
    logrun "rm $ZIPFILENAME"
    logrun "cd $TEMP_DIR/paradiseo-3.0.0-beta"
    logrun "pwd"
else
    echo 'Error on downloading the .zip file'
    exit 1
fi

logrun 'cmake . -DEO_ONLY=1 -DCMAKE_CXX_STANDARD=17'
logrun 'cmake --build . --target install'
logrun "cd $START_DIR"

delete_temp_dir
exit 0