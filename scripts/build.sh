#!/bin/bash

source "$(pwd)/$(dirname "$0")/utils.sh"

echo '> cmake -S . -B build -Wno-dev -DCMAKE_EXPORT_COMPILE_COMMANDS=1'
cmake -S . -B build -Wno-dev -DCMAKE_EXPORT_COMPILE_COMMANDS=1

echo '> cmake --build build --target solvecvrp'
cmake --build build --target solvecvrp
cp build/src/cpp/solvecvrp solvecvrp
