#!/bin/bash

numexperiments=50
databasename="/data/results-$(date +"%Y%m%d%H%M%S").db"

crossovers=(OX CX MOX LOX PMX PPX AEX GOX GX)
instances=(
    'cvrp-0-pa-100' 'cvrp-0-pa-116' 'cvrp-0-pa-117' 'cvrp-0-pa-118'
    'cvrp-0-df-30'  'cvrp-0-df-35'  'cvrp-0-df-42'  'cvrp-0-df-100'
    'cvrp-2-rj-13'  'cvrp-2-rj-57'  'cvrp-2-rj-75'  'cvrp-2-rj-17'
)

for instance in ${instances[@]}; do
    for x in ${crossovers[@]}; do
        echo '----------------------------------------------------------------------------------------'
        echo "![${numexperiments}x] Crossover: ${x}; Instance: ${instance}"
        echo '----------------------------------------------------------------------------------------'
        solvecvrp --algorithm=ga --crossover=${x} --repeat=${numexperiments} --database=${databasename} "/instances/masters-thesis/${instance}.json"
    done 
done
