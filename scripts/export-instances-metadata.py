import os
import json
import pandas as pd

data = pd.DataFrame()

for dir in os.listdir("dataset"):
    for subdir in os.listdir(f"dataset/{dir}/cvrp-instances-1.0"):
        for region in os.listdir(f"dataset/{dir}/cvrp-instances-1.0/{subdir}"):
            for filename in os.listdir(f"dataset/{dir}/cvrp-instances-1.0/{subdir}/{region}"):
                instance = json.loads(open(f"dataset/{dir}/cvrp-instances-1.0/{subdir}/{region}/{filename}", "r").read())
                data = pd.concat([data, pd.DataFrame({
                    "dir": [subdir],
                    "region": [region],
                    "instance_name": [instance["name"]],
                    "num_deliveries": [len(instance["deliveries"])],
                    "vehicle_capacity": [instance["vehicle_capacity"]],
                })])

print(data)
data.to_csv("all-instances.csv", index=True)