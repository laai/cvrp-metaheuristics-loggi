#!/bin/sh
#
# Download all OSRM data provided by Loggi(TM) in the cloud.

targetdir=$1
url='https://loggibud.s3.amazonaws.com/osrm/osrm.zip'

if [[ -f ${targetdir}/osrm/brazil-201110.osrm ]]; then
    echo "OSRM main file already exists"
else
    echo "Downloading $url [...]"

    curl -# -SL $url -o osrm.zip
    if [ $? -ne 0 ]; then
        echo "Failed on download data from $url"
        exit 1
    fi

    echo "Unpacking osrm.zip into osrm/"
    unzip osrm.zip -d $targetdir > /dev/null
    if [ $? -ne 0 ]; then
        echo 'Failed on unzipping osrm.zip'
        exit 1
    fi

    rm -v osrm.zip
fi
