-- View a list with number of deliveries per vehicle in the solution.
SELECT JSON_GROUP_ARRAY( JSON_ARRAY_LENGTH(vehicles.value, "$.deliveries") ) AS num_deliveries_per_vehicle
	FROM experiment_results er, JSON_EACH(JSON_EXTRACT("solution", "$.vehicles")) AS vehicles
	GROUP BY er.id;

-- Overview of the results
SELECT
	algorithm_name as "algorithm",
	instance_name as "instance",
	COUNT(id) as "count",
	ROUND(AVG(final_total_distance)) as "mean",
	ROUND(STDEV(final_total_distance)) as "stdev",
	ROUND(MIN(final_total_distance)) as "min",
	ROUND(MAX(final_total_distance)) as "max",
	AVG(solution_num_vehicles) as "mean_num_vehicles"
FROM experiment_results er
GROUP BY 1, 2;

SELECT
	algorithm_name,
	instance_name,
	count(id) as "count",
	round(avg(final_total_distance)) as "avg_final_distance",
	(round(avg(elapsed_time_ms)) / 1000) / 60 as "avg_time_in_minutes"
FROM experiment_results er
GROUP BY 1, 2;
